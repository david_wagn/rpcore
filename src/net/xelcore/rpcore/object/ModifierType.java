package net.xelcore.rpcore.object;

public enum ModifierType {

    SILENCED, STUNNED, MUTED
}
