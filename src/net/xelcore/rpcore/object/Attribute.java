package net.xelcore.rpcore.object;

public enum Attribute {

    NONE, STRENGTH, AGILITY, INTELLIGENCE
}
