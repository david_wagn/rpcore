package net.xelcore.rpcore.object;

import org.bukkit.Material;

import java.util.HashMap;

public class Job {

    private Integer id;
    private String name;
    private String description;
    private Material material;
    private JobType jobType;
    private Integer baseXp;
    private Double xpGain;

    public static HashMap<Integer, Job> jobList = new HashMap<>();

    public Job(Integer id, String name, String description, Material material, JobType jobType, Integer baseXp, Double xpGain) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.material = material;
        this.jobType = jobType;
        this.baseXp = baseXp;
        this.xpGain = xpGain;
        jobList.put(this.id, this);
    }

    public static Job fromType(JobType t) {
        for(Job j : jobList.values()) {
            if(j.getJobType() == t) {
                return j;
            }
        }
        return null;
    }

    public Double getXpGain() {
        return xpGain;
    }

    public Integer getBaseXp() {
        return baseXp;
    }

    public JobType getJobType() {
        return jobType;
    }

    public String getName() {
        return name;
    }

    public Integer getId() {
        return id;
    }

    public Material getMaterial() {
        return material;
    }

    public String getDescription() {
        return description;
    }
}
