package net.xelcore.rpcore.object;

import org.bukkit.Material;

import java.util.HashMap;
import java.util.List;

public class RpClass {

    private int id;
    private BaseClass baseClass;
    private String name;
    private String description;
    private String colorCode;
    private Material material;
    private List<Double> baseAttributes;
    private List<Double> attributeGain;
    private List<Ability> abilities;

    public static HashMap<Integer, RpClass> classList = new HashMap<>();

    public RpClass(Integer id, BaseClass baseClass, String name, String description, String colorCode, Material material, List<Double> baseAttributes, List<Double> attributeGain, List<Ability> abilities) {
        this.id = id;
        this.baseClass = baseClass;
        this.name = name;
        this.description = description;
        this.colorCode = colorCode;
        this.material = material;
        this.baseAttributes = baseAttributes;
        this.attributeGain = attributeGain;
        this.abilities = abilities;
        classList.put(this.id, this);
    }

    public Attribute getPrimaryAttribute() {
        return this.baseClass.getPrimaryAttribute();
    }

    public int getId() {
        return id;
    }

    public BaseClass getBaseClass() {
        return baseClass;
    }

    public String getName() {
        return name;
    }

    public Double getBaseStrength() {
        return this.getBaseAttributes().get(0);
    }

    public Double getBaseAgility() {
        return this.getBaseAttributes().get(1);
    }

    public Double getBaseIntelligence() {
        return this.getBaseAttributes().get(2);
    }

    public Double getStrengthGain() {
        return this.getAttributeGain().get(0);
    }

    public Double getAgilityGain() {
        return this.getAttributeGain().get(1);
    }

    public Double getIntelligenceGain() {
        return this.getAttributeGain().get(2);
    }

    public List<Double> getBaseAttributes() {
        return baseAttributes;
    }

    public List<Double> getAttributeGain() {
        return attributeGain;
    }

    public String getDescription() {
        return description;
    }

    public List<Ability> getAbilities() {
        return abilities;
    }

    public String getColorCode() {
        return colorCode;
    }

    public Material getMaterial() {
        return material;
    }
}
