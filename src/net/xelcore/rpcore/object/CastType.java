package net.xelcore.rpcore.object;

public enum CastType {

    SELF, ALLY, ENEMY, AOE, AURA, RADIUS, DIRECTION, BLOCK
}
