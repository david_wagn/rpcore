package net.xelcore.rpcore.object;

import org.bukkit.Material;

import java.util.HashMap;

public class BaseClass {

    private Integer id;
    private String name;
    private Attribute primaryAttribute;
    private String colorCode;
    private Material material;

    public static HashMap<Integer, BaseClass> classList = new HashMap<>();

    public BaseClass(Integer id, String name, Attribute primaryAttribute, String colorCode, Material material) {
        this.id = id;
        this.name = name;
        this.primaryAttribute = primaryAttribute;
        this.colorCode = colorCode;
        this.material = material;
        classList.put(this.id, this);
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Attribute getPrimaryAttribute() {
        return primaryAttribute;
    }

    public String getColorCode() {
        return colorCode;
    }

    public Material getMaterial() {
        return material;
    }
}
