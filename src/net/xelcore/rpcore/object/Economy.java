package net.xelcore.rpcore.object;

import net.xelcore.rpcore.data.EconomyData;

public class Economy {

    private RpPlayer rpPlayer;
    private Integer gold;

    public Economy(RpPlayer rp) {
        this.rpPlayer = rp;
    }

    public Economy load(EconomyData ed) {
        this.gold = ed.getGold();
        return this;
    }

    public void transfer(RpPlayer target, int amount) {
        rpPlayer.removeGold(amount);
        target.addGold(amount);
    }

    public void add(int amount) {
        rpPlayer.addGold(amount);
    }

    public void remove(int amount) {
        rpPlayer.removeGold(amount);
    }

    public void set(int amount) {
        rpPlayer.setGold(amount);
    }

    public Integer getGold() {
        return gold;
    }
}
