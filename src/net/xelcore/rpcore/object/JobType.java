package net.xelcore.rpcore.object;

public enum JobType {

    HUNTER, MINER, LUMBERJACK, FISHER, FARMER, ALCHEMIST, SHEPHERD

}
