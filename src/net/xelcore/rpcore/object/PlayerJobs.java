package net.xelcore.rpcore.object;

import net.xelcore.rpcore.data.PlayerJobData;
import org.bukkit.entity.Player;

import java.util.HashMap;

public class PlayerJobs {

    private Player player;
    private HashMap<Job, PlayerJob> jobs = new HashMap<>();

    public PlayerJobs(RpPlayer rp) {
        this.player = rp.getPlayer();
    }

    public PlayerJobs load(PlayerJobData pjd) {
        for(Job j : Job.jobList.values()) {
            jobs.put(j, new PlayerJob(pjd.getRpPlayer(), j, pjd.getJobLevel(j), pjd.getJobXp(j)));
        }
        return this;
    }

    public Player getPlayer() {
        return player;
    }

    public HashMap<Job, PlayerJob> getJobs() {
        return jobs;
    }

    public PlayerJob getJob(Job job) {
        return jobs.get(job);
    }

}
