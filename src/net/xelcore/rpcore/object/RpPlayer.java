package net.xelcore.rpcore.object;

import net.xelcore.rpcore.data.PlayerData;
import net.xelcore.rpcore.main.Utils;
import net.xelcore.rplib.message.Actionbar;
import net.xelcore.rplib.message.Message;
import net.xelcore.rplib.message.Title;
import net.xelcore.rplib.tablist.Tablist;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

public class RpPlayer {

    private Player player;
    private RpClass rpClass;
    private long joined;
    private Timer timer;
    private Integer gold;

    private double strength;
    private double agility;
    private double intelligence;

    private double mana;
    private double manaRegenAmp;
    private double hpRegenAmp;

    private double magicResistance;
    private double armor;

    private int skillPoints;
    private int classPoints;

    private int level;
    private int xp;
    private double xpMultiplier;

    private boolean castingAbility;
    private Integer castSlot;
    private Integer[] castSlots;
    private HashMap<Integer, Ability> castableAbilities;
    private HashMap<Ability, Long> coolDown = new HashMap<>();
    private ArrayList<Ability> passiveAbilities = new ArrayList<>();
    private HashMap<Ability, Integer> skills = new HashMap<>();

    private HashMap<Job, PlayerJob> jobs = new HashMap<>();

    private HashMap<ModifierType, Long> modifiers = new HashMap<>();

    private static HashMap<String, RpPlayer> playerList = new HashMap<>();

    /* General */
    public RpPlayer(Player p) {
        this.player = p;
    }
    public void startInterval() {
        this.timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {

                for(ModifierType mt : ModifierType.values()) {
                    if(modifiers.containsKey(mt)) {
                        if(modifiers.get(mt) < System.currentTimeMillis()/1000) {
                            modifiers.remove(mt);
                        }
                    }
                }

                if(isMuted()) new Title(player, " ", "§8☓ §5Du bist gemuted §8☓", 0, 20, 20, 0, 20, 20);
                if(isSilenced()) new Title(player, " ", "§8☓ §5Du bist verstummt §8☓", 0, 20, 20, 0, 20, 20);
                if(isStunned()) new Title(player, " ", "§8☓ §5Du bist gestunnt §8☓", 0, 20, 20, 0, 20, 20);

                player.setMaxHealth(getMaxHealth());

                if (getMana() >= getMaxMana()) setMana(getMaxMana());
                else setMana(getMana() + getManaRegen());

                if (player.getHealth() > player.getMaxHealth() || player.getHealth() + getHpRegen() >= player.getMaxHealth())
                    player.setHealth(player.getMaxHealth());
                else player.setHealth(player.getHealth() + getHpRegen());

                new Tablist(player, "\n§3Roleplay-Server\n§7" + Bukkit.getOnlinePlayers().size() + "§8/§7" + Bukkit.getMaxPlayers() + "\n ", "\n§7Hosted by §6Xelcore.net\n ").send();
                new Actionbar(player, getActionBar());

                if (!player.isOnline()) this.cancel();
                if (player.isDead()) {
                    this.cancel();
                }
            }
        }, 100, 100);
    }
    public Player getPlayer() {
        return this.player;
    }
    public Timer getTimer() {
        return timer;
    }
    public String getActionBar() {
        if (isCastingAbility()) {
            if(isSilenced() ||isStunned()) {
                setCastingAbility(false);
                new Message(player).errorprefix("Du bist " + (isSilenced() ? "verstummt":"") + (isStunned() ? "gestunnt":""));
            }
            boolean first = true;
            int count = 0;
            StringBuilder actionbar = new StringBuilder();
            castableAbilities = new HashMap<>();
            for (Ability a : getRpClass().getAbilities()) {
                if (a.getAbilityType() == AbilityType.ACTIVE) {
                    if (skills.get(a) > 0) {
                        if (first) {
                            first = false;
                            actionbar.append("§a§l§n").append(getCastSlots()[count] + 1).append("§r §6").append(getMana() < a.getManacost() || isOnCooldown(a) ? "§c§m" : "").append(a.getName()).append("§r");
                        } else {
                            actionbar.append(" §8| §a§l§n").append(getCastSlots()[count] + 1).append("§r §6").append(getMana() < a.getManacost() || isOnCooldown(a) ? "§c§m" : "").append(a.getName()).append("§r");
                        }
                        if (a.getManacost() >= 0) {
                            actionbar.append(" §8[§b✦ ").append(getMana() < a.getManacost() ? "§c" : "").append(Utils.format(a.getManacost() + (a.getManacost() * a.getLevelAmp()))).append("§8]");
                        }
                        if (isOnCooldown(a)) {
                            actionbar.append(" §8[§d").append(Utils.format(getCoolDownLeft(a))).append("§8]");
                        }
                        castableAbilities.put(getCastSlots()[count], a);
                    }
                    count++;
                }
            }
            if (castableAbilities.size() == 0) {
                actionbar = new StringBuilder("§7Du hast noch keine Fähigkeiten geskillt §8| §8[§a§l§nF§r§8] §7Zurück");
            } else {
                actionbar.append(" §8| §8[§a§l§nF§r§8] §7Abbrechen");
            }

            return actionbar.toString();
        } else {
            return "§c❤ §7" + Utils.format(getPlayer().getHealth()) + "§8/§7" + Utils.format(getPlayer().getMaxHealth()) + " §8| " +
                    "§b✦ §7" + Utils.format(getMana()) + "§8/§7" + Utils.format(getMaxMana()) + " §8| " +
                    "§a⛨ " + Utils.format(getArmor()) + " §8- " +
                    "§5✸ " + Utils.format(getMagicResistance()) + "%";
        }
    }
    public static RpPlayer getPlayerByUUID(String uuid) {
        return playerList.get(uuid);
    }
    public static RpPlayer getPlayer(Player p) {
        return playerList.get(p.getUniqueId().toString());
    }
    public static RpPlayer getPlayerByName(String name) {
        if (Bukkit.getPlayer(name).isOnline())
            return playerList.get(Bukkit.getPlayer(name).getUniqueId().toString());
        else
            return null;
    }
    public static HashMap<String, RpPlayer> getPlayerList() {
        return playerList;
    }

    /* Playerdata */
    public void save() {
        PlayerData.save(this);
    }
    public RpPlayer load(PlayerData pd) {
        this.rpClass = pd.getRpClass();
        this.strength = pd.getStrength();
        this.agility = pd.getAgility();
        this.intelligence = pd.getIntelligence();
        this.skillPoints = pd.getSkillPoints();
        this.classPoints = pd.getClassPoints();
        this.level = pd.getLevel();
        this.xp = pd.getXp();
        playerList.put(player.getUniqueId().toString(), this);
        return this;
    }

    /* Economy */
    public Integer getGold() {
        return this.gold;
    }
    public void setGold(int amount) {
        this.gold = amount;
    }
    public boolean hasGold(int amount) {
        return this.gold >= amount;
    }
    public void addGold(int amount) {
        this.gold += amount;
    }
    public void removeGold(int amount) {
        this.gold -= amount;
        if(this.gold < 0) {
            this.gold = 0;
        }
    }

    /* Playtime */
    public void setJoined(long joined) {
        this.joined = joined;
    }
    public long getJoined() {
        return joined;
    }

    /* Modifiers */
    public boolean isStunned() {
        return modifiers.containsKey(ModifierType.STUNNED);
    }
    public boolean isSilenced() {
        return modifiers.containsKey(ModifierType.SILENCED);
    }
    public boolean isMuted() {
        return modifiers.containsKey(ModifierType.MUTED);
    }
    public void setModifier(ModifierType modifierType, double duration) {
        modifiers.remove(modifierType);
        modifiers.put(modifierType, (long) ((System.currentTimeMillis()/1000)+duration));
    }
    public void dispell() {
        for(ModifierType mt : ModifierType.values()) {
            modifiers.remove(mt);
        }
    }

    /* Abilities */
    public HashMap<Ability, Long> getCoolDown() {
        return this.coolDown;
    }
    public void setCoolDown(Ability a, Long l) {
        coolDown.put(a, l);
    }
    public boolean isOnCooldown(Ability a) {
        if (getCoolDown().containsKey(a)) {
            return getCoolDown().get(a) + (a.getCooldown() * 1000) > System.currentTimeMillis();
        } else {
            return false;
        }
    }
    public void setOnCooldown(Ability a) {
        if (!this.coolDown.containsKey(a)) {
            this.coolDown.remove(a);
        }
        this.coolDown.put(a, System.currentTimeMillis());
    }
    public ArrayList<Ability> getPassiveAbilities() {
        return this.passiveAbilities;
    }
    public double getCoolDownLeft(Ability a) {
        return ((getCoolDown().get(a) + (a.getCooldown() * 1000)) - System.currentTimeMillis()) / 1000;
    }
    public Integer getCastSlot() {
        return castSlot;
    }
    public boolean isCastingAbility() {
        return castingAbility;
    }
    public void setSkills(PlayerSkills ps) {
        this.skills = ps.getSkills();
    }
    public Integer[] getCastSlots() {
        return castSlots;
    }
    public void setCastSlots(Integer[] castSlots) {
        this.castSlots = castSlots;
    }
    public HashMap<Integer, Ability> getCastableAbilities() {
        return castableAbilities;
    }
    public HashMap<Ability, Integer> getSkills() {
        return skills;
    }
    public void setCastingAbility(boolean castingAbility) {
        this.castingAbility = castingAbility;
    }
    public void setCastSlot(Integer castSlot) {
        this.castSlot = castSlot;
    }

    /* Attributes */
    public void setStrength(double strength) {
        this.strength = strength;
    }
    public void setAgility(double agility) {
        this.agility = agility;
    }
    public void setIntelligence(double intelligence) {
        this.intelligence = intelligence;
    }
    public double getStrength() {
        return strength;
    }
    public double getAgility() {
        return agility;
    }
    public double getIntelligence() {
        return intelligence;
    }

    /* Points */
    public void setClassPoints(int classPoints) {
        this.classPoints = classPoints;
    }
    public void setSkillPoints(int skillPoints) {
        this.skillPoints = skillPoints;
    }
    public int getSkillPoints() {
        return skillPoints;
    }
    public int getClassPoints() {
        return classPoints;
    }

    /* Leveling */
    public void setXp(Integer xp) {
        this.xp = xp;
    }
    public void setLevel(int level) {
        this.level = level;
    }
    public int getLevel() {
        return level;
    }
    public int getXp() {
        return xp;
    }
    public Integer getLevelProgress() {
        return (getXp() * 100) / xpNeeded();
    }
    public Integer xpNeeded() {
        return ((220 + (100 * (this.level / 10))) * (this.level == 0 ? 1 : this.level));
    }
    public float mapXp() {
        double map = (double) this.xp / this.xpNeeded();
        if (map > 1.0) {
            map = 0.999;
        }
        return (float) map;
    }
    public void levelUp() {
        setStrength(getStrength() + getRpClass().getStrengthGain());
        setAgility(getAgility() + getRpClass().getAgilityGain());
        setIntelligence(getIntelligence() + getRpClass().getIntelligenceGain());
        setLevel(getLevel() + 1);
        this.xp = 0;
    }
    public double getXpMultiplier() {
        return xpMultiplier;
    }
    public void setXpMultiplier(double xpMultiplier) {
        this.xpMultiplier = xpMultiplier;
    }

    /* Jobs */
    public void setJobs(HashMap<Job, PlayerJob> jobs) {
        this.jobs = jobs;
    }
    public HashMap<Job, PlayerJob> getJobs() {
        return jobs;
    }

    /* Class */
    public RpClass getRpClass() {
        return rpClass;
    }
    public void setRpClass(RpClass rpClass) {
        this.rpClass = rpClass;
        this.strength = getRpClass().getBaseStrength();
        this.agility = getRpClass().getBaseAgility();
        this.intelligence = rpClass.getBaseIntelligence();
        getPlayer().kickPlayer("§6Deine Klasse ist jetzt " + rpClass.getColorCode() + rpClass.getName() + "\n§7Bitte verbinde erneut");
    }

    /* Stats */
    public double getMaxHealth() {
        return (this.strength / 2);
    }
    public double getHpRegen() {
        return (hpRegenAmp * (strength / 500));
    }
    public double getHpRegenAmp() {
        return hpRegenAmp;
    }
    public void setHpRegenAmp(double hpRegenAmp) {
        this.hpRegenAmp = hpRegenAmp;
    }

    public double getMaxMana() {
        return (this.intelligence * 0.35);
    }
    public double getMana() {
        return mana;
    }
    public void setMana(double mana) {
        this.mana = mana;
    }
    public double getManaRegen() {
        return (manaRegenAmp * (intelligence / 80));
    }
    public void setManaRegenAmp(double manaRegenAmp) {
        this.manaRegenAmp = manaRegenAmp;
    }
    public double getManaRegenAmp() {
        return manaRegenAmp;
    }

    public double getArmor() {
        return armor;
    }
    public double getDamageReduction(double initialDamage) {
        return (initialDamage * ((getArmor() / 2) / 30));
    }
    public void addArmor(double armor) {
        this.armor += armor;
    }
    public void setArmor(double armor) {
        if(armor < 0) armor = 0;
        this.armor = armor;
    }
    public void removeArmor(double armor) {
        if(this.armor < 0) {
            this.armor = 0;
        } else {
            this.armor-=armor;
        }
    }
    public void resetArmor() {
        armor = (this.agility / 4);
    }

    public double getMagicDamageReduction(double initialDamage) {
        return initialDamage-(initialDamage*(getMagicResistance()/100));
    }
    public void addMagicResistance(double resistance) {
        this.magicResistance+=resistance;
    }
    public void removeMagicResistance(double resistance) {
        if (this.magicResistance < 0) {
            this.magicResistance = 0;
        } else {
            this.magicResistance -= resistance;
        }
    }
    public void resetMagicResistance() {
        this.magicResistance = (strength/8);
    }
    public Double getMagicResistance() {
        return magicResistance;
    }
}
