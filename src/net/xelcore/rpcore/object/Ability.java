package net.xelcore.rpcore.object;

import net.xelcore.rpcore.ability.ActiveExecutor;
import net.xelcore.rpcore.main.Main;
import net.xelcore.rpcore.main.Utils;
import net.xelcore.rplib.message.Message;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;

public class Ability {

    private Integer id;
    private String name;
    private AbilityType abilityType;
    private CastType castType;
    private String description;
    private Double levelAmp;
    private String value;
    private Double duration;
    private Double cooldown;
    private Double manacost;

    public static HashMap<Integer, Ability> abilities = new HashMap<>();

    public Ability(Integer id, String name, AbilityType abilityType, CastType castType, String description, Double levelAmp, String value, Double duration, Double cooldown, Double manacost) {
        this.id = id;
        this.name = name;
        this.abilityType = abilityType;
        this.castType = castType;
        this.description = description;
        this.levelAmp = levelAmp;
        this.value = value;
        this.duration = duration;
        this.cooldown = cooldown;
        this.manacost = manacost;
        abilities.put(this.id, this);
    }

    public void execute(Player caster, Entity target, Location loc) {
        if (this.abilityType == AbilityType.ACTIVE) {
            try {
                Method m = ActiveExecutor.class.getMethod(getFormattedName(), Ability.class, Player.class, Entity.class, Location.class);
                if(m != null) {
                    RpPlayer rp = RpPlayer.getPlayer(caster);
                    if(!rp.isOnCooldown(this)) {
                        if(rp.getMana() > getManacost()) {
                            m.invoke("cast", new Object[]{abilities.get(this.id), caster, target, loc});
                        } else {
                            new Message(caster).errorprefix("Du hast nicht genug Mana");
                        }
                    } else {
                        new Message(caster).errorprefix("Diese Fähigkeit hat noch §6" + Utils.format(rp.getCoolDownLeft(this)) + " Abklingzeit");
                    }
                } else {
                    Main.log.error("No executor method for ability " + abilities.get(this.id).getName() + ", can't execute ability");
                }
            } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
            }
        }
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getFormattedName() {
        return this.name.replaceAll(" ", "_").toLowerCase();
    }

    public Double getCooldown() {
        return cooldown;
    }

    public Double getLevelAmp() {
        return levelAmp;
    }

    public String getDescription() {
        return description;
    }

    public AbilityType getAbilityType() {
        return abilityType;
    }

    public CastType getCastType() {
        return castType;
    }

    public Double getManacost() {
        return manacost;
    }

    public Double getDuration() {
        return duration;
    }

    public String getValue() {
        return value;
    }

    public Integer getValue(Integer index) {
        return Integer.valueOf(this.value.split("/")[index]);
    }

    public Double[] getValueScaling() {
        return Utils.castToDoubleArray(this.value.split("/"));
    }
}
