package net.xelcore.rpcore.object;

import net.xelcore.rpcore.data.SkillData;

import java.util.HashMap;

public class PlayerSkills {

    private RpPlayer rpPlayer;
    private SkillData d;
    private HashMap<Ability, Integer> skills = new HashMap<>();
    public static HashMap<RpPlayer, PlayerSkills> skillsList = new HashMap<>();

    public PlayerSkills(RpPlayer rp) {
        this.rpPlayer = rp;
    }

    public PlayerSkills load(SkillData d) {
        this.d = d;
        this.skills = d.getSkills();
        skillsList.put(this.rpPlayer, this);
        return this;
    }

    public Integer getAbilityLevel(Ability a) {
        return this.skills.getOrDefault(a, null);
    }

    public void setAbilityLevel(Ability a, Integer level) {
        if(this.skills.containsKey(a)) {
            this.skills.remove(a);
            this.skills.put(a, level);
        }
    }

    public void save() {
        SkillData.save(this.rpPlayer);
    }

    public RpPlayer getRpPlayer() {
        return rpPlayer;
    }

    public SkillData getD() {
        return d;
    }

    public HashMap<Ability, Integer> getSkills() {
        return skills;
    }
}
