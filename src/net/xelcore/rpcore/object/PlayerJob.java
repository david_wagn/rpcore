package net.xelcore.rpcore.object;

import org.bukkit.entity.Player;

import java.util.HashMap;

public class PlayerJob {

    private RpPlayer rp;
    private Player player;
    private Job job;
    private Integer xp;
    private Integer level;
    public static HashMap<RpPlayer, PlayerJob> playerJobList = new HashMap<>();

    public PlayerJob(RpPlayer rpPlayer, Job job, Integer level, Integer xp) {
        this.rp = rpPlayer;
        this.player = rp.getPlayer();
        this.job = job;
        this.xp = xp;
        this.level = level;
        playerJobList.put(this.rp, this);
    }

    public Player getPlayer() {
        return player;
    }

    public RpPlayer getRpPlayer() {
        return rp;
    }

    public void setXp(int xp) {
        this.xp = xp;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public Job getJob() {
        return job;
    }

    public Integer getXp() {
        return xp;
    }

    public Integer getXpNeeded() {
        return (50 + (75 * (level / 10))) * (level == 0 ? 1 : level);
    }

    public Integer getLevelProgress() {
        return (getXp() * 100) / getXpNeeded();
    }

    public Integer getLevel() {
        return level;
    }
}
