package net.xelcore.rpcore.ability;

import net.xelcore.rpcore.event.PlayerPassiveTriggerEvent;
import net.xelcore.rpcore.main.Utils;
import net.xelcore.rpcore.object.Ability;
import net.xelcore.rpcore.object.RpPlayer;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

public class PassiveExecutor implements Listener {

    @EventHandler
    public void entityDamage(EntityDamageEvent e) {
        if(e.getEntity() instanceof Player) {
            Player p = (Player) e.getEntity();
            RpPlayer rp = RpPlayer.getPlayer(p);
            for(Ability a : rp.getPassiveAbilities()) {
                if(a.getFormattedName().equalsIgnoreCase("ignore_falldamage")) {
                    if(rp.getSkills().get(a) > 0) {
                        if(e.getCause() == EntityDamageEvent.DamageCause.FALL) {
                            if(Utils.chance(a.getValue(rp.getSkills().get(a)-1))) {
                                e.setCancelled(true);
                                Bukkit.getPluginManager().callEvent(new PlayerPassiveTriggerEvent(p, null, a, p.getLocation()));
                            }
                        }
                    }
                }
            }
        }
    }

}
