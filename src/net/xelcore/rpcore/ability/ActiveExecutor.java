package net.xelcore.rpcore.ability;

import net.xelcore.rpcore.event.PlayerCastAbilityEvent;
import net.xelcore.rpcore.object.Ability;
import net.xelcore.rpcore.object.RpPlayer;
import net.xelcore.rplib.message.Message;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

public class ActiveExecutor {


    public static void active_1(Ability ability, Player caster, Entity target, Location loc) {
        RpPlayer rp = RpPlayer.getPlayer(caster);
        if(target instanceof Damageable) {
            rp.setCastingAbility(false);
            rp.setOnCooldown(ability);
            rp.setMana(rp.getMana()-ability.getManacost());
            Damageable d = (Damageable) target;
            d.damage(d.getHealth(), caster);
            d.setHealth(0);
            Bukkit.getPluginManager().callEvent(new PlayerCastAbilityEvent(caster, target, ability, loc));
        } else {
            new Message(caster).errorprefix("Kein Ziel gefunden");
        }
    }

    public static void active_2(Ability ability, Player caster, Entity target, Location loc) {
        System.out.println("Player castts ability 2 " + caster.getName());
    }

}
