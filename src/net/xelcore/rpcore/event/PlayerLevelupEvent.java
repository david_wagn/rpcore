package net.xelcore.rpcore.event;

import net.xelcore.rpcore.main.Main;
import net.xelcore.rpcore.object.RpPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class PlayerLevelupEvent extends Event {

    private Player player;
    private RpPlayer rpPlayer;
    private int prevLevel;
    private int nextLevel;

    public PlayerLevelupEvent(Player player, RpPlayer rpPlayer, Integer prevLevel, Integer nextLevel) {
        this.player = player;
        this.rpPlayer = rpPlayer;
        this.prevLevel = prevLevel;
        this.nextLevel = nextLevel;
        Main.log.debug("rpcore>LevelUpEvent", "Player " + player.getName() + " leveled up to level " + nextLevel);
    }

    public RpPlayer getRpPlayer() {
        return this.rpPlayer;
    }

    public Player getPlayer() {
        return this.player;
    }

    public int getPrevLevel() {
        return prevLevel;
    }

    public int getNextLevel() {
        return nextLevel;
    }

    private static final HandlerList HANDLERS = new HandlerList();

    public HandlerList getHandlers() {
        return HANDLERS;
    }

    public static HandlerList getHandlerList() {
        return HANDLERS;
    }


}
