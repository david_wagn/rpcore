package net.xelcore.rpcore.event;

import net.xelcore.rpcore.main.Main;
import net.xelcore.rpcore.object.Ability;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class PlayerCastAbilityEvent extends Event {

    private Player caster;
    private Entity target;
    private Ability ability;
    private Location location;

    public PlayerCastAbilityEvent(Player caster, Entity target, Ability ability, Location location) {
        this.caster = caster;
        this.target = target;
        this.ability = ability;
        this.location = location;
        String call = Main.log.getCall();
        Main.log.debug("rpcore>AbilityCaster", "-----[" + call + "]-----");
        Main.log.debug("rpcore>AbilityCaster", "Ability cast overview");
        Main.log.debug("rpcore>AbilityCaster", "Caster: " + caster);
        Main.log.debug("rpcore>AbilityCaster", "Target: " + target.toString());
        Main.log.debug("rpcore>AbilityCaster", "Ability: " + ability.getName());
        Main.log.debug("rpcore>AbilityCaster", "Location: " + location.toString());
        Main.log.debug("rpcore>AbilityCaster", "-----[" + call + "]-----");
    }


    public Player getCaster() {
        return caster;
    }

    public Entity getTarget() {
        return target;
    }

    public Ability getAbility() {
        return ability;
    }

    public Location getLocation() {
        return location;
    }

    private static final HandlerList HANDLERS = new HandlerList();

    public HandlerList getHandlers() {
        return HANDLERS;
    }

    public static HandlerList getHandlerList() {
        return HANDLERS;
    }

}
