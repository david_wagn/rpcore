package net.xelcore.rpcore.event;

import net.xelcore.rpcore.object.RpPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class PlayerToggleAbilitySelectionEvent extends Event {

    private Player player;
    private RpPlayer rpPlayer;
    private Integer selectionSlot;

    public PlayerToggleAbilitySelectionEvent(Player player, RpPlayer rpPlayer, Integer selectionSlot) {
        this.player = player;
        this.rpPlayer = rpPlayer;
        this.selectionSlot = selectionSlot;
    }

    public Player getPlayer() {
        return player;
    }

    public RpPlayer getRpPlayer() {
        return rpPlayer;
    }

    public Integer getSelectionSlot() {
        return selectionSlot;
    }

    private static final HandlerList HANDLERS = new HandlerList();

    public HandlerList getHandlers() {
        return HANDLERS;
    }

    public static HandlerList getHandlerList() {
        return HANDLERS;
    }
}
