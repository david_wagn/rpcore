package net.xelcore.rpcore.command;

import net.xelcore.rpcore.inventory.ClassInventory;
import net.xelcore.rpcore.main.Main;
import net.xelcore.rpcore.object.BaseClass;
import net.xelcore.rpcore.object.RpClass;
import net.xelcore.rpcore.object.RpPlayer;
import net.xelcore.rplib.message.Message;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ClassCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(sender instanceof Player) {
            Player p = (Player) sender;
            if(args.length == 0) {
                RpPlayer rp = RpPlayer.getPlayer(p);
                if(rp.getRpClass().getBaseClass() == BaseClass.classList.get(0) && rp.getRpClass() == RpClass.classList.get(0)) {
                    if(rp.getClassPoints() != 0) {
                        new ClassInventory(p);
                    } else {
                        new Message(p).errorprefix("Du hast nicht genügend Klassenpunkte");
                    }
                } else {
                    new Message(p).errorprefix("Du hast bereits eine Klasse ausgewählt");
                }
            } else {
                new Message(p).wrongcmdusage("/class");
            }
        } else {
            Main.log.error("Only players can execute this command");
        }
        return true;
    }
}
