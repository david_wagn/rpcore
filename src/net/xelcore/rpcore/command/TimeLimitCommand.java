package net.xelcore.rpcore.command;

import net.xelcore.rpcore.main.Main;
import net.xelcore.rplib.message.Message;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TimeLimitCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(sender instanceof Player) {
            Player p = (Player) sender;
            if(p.hasPermission("rp.core.timelimit")) {
                if(args.length == 0) {
                    Main.limitTime = !Main.limitTime;
                    new Message(p).prefix(Main.limitTime ? "Spieler haben nun limitierte Spielzeit" : "Spieler haben nun keine limitierte Spielzeit");
                } if(args.length == 1) {
                    if(args[0].equalsIgnoreCase("on")) {
                        Main.limitTime = true;
                        new Message(p).prefix("Spieler haben nun limitierte Spielzeit");
                    } else if(args[0].equalsIgnoreCase("off")) {
                        Main.limitTime = false;
                        new Message(p).prefix("Spieler haben nun keine limitierte Spielzeit");
                    } else {
                        new Message(p).wrongcmdusage("Bitte verwende /timelimit <on/off>");
                    }
                } else {
                    new Message(p).wrongcmdusage("/timelimit <on/off>");
                }
            } else {
                new Message(p).noperms();
            }
        } else {
            if(args.length == 0) {
                Main.limitTime = !Main.limitTime;
                sender.sendMessage(Main.limitTime ? "Spieler haben nun limitierte Spielzeit" : "Spieler haben nun keine limitierte Spielzeit");
            } else if(args.length == 1) {
                if(args[0].equalsIgnoreCase("on")) {
                    Main.limitTime = true;
                    sender.sendMessage("Spieler haben nun limitierte Spielzeit");
                } else if(args[0].equalsIgnoreCase("off")) {
                    Main.limitTime = false;
                    sender.sendMessage("Spieler haben nun keine limitierte Spielzeit");
                } else {
                    sender.sendMessage("Bitte verwende /timelimit <on/off>");
                }
            }
        }
        return true;
    }
}
