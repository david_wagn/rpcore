package net.xelcore.rpcore.command;

import net.xelcore.rpcore.main.Main;
import net.xelcore.rpcore.main.Utils;
import net.xelcore.rpcore.object.RpPlayer;
import net.xelcore.rplib.message.Message;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class PlaytimeCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(sender instanceof Player) {
            Player p = (Player) sender;
            if(args.length == 0) {
                RpPlayer rp = RpPlayer.getPlayer(p);
                new Message(p).prefix("Du hast noch §6" + ((double) ((((rp.getJoined() + Main.onlineTime) - System.currentTimeMillis() / 1000) * 60) / 360) /10) + " §6Minuten §7Spielzeit");
            } else {
                new Message(p).wrongcmdusage("/playtime");
            }
        } else {
            Main.log.error("Only players can execute this command");
        }
        return true;
    }
}
