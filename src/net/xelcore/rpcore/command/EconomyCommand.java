package net.xelcore.rpcore.command;

import net.xelcore.rpcore.main.Main;
import net.xelcore.rpcore.object.Economy;
import net.xelcore.rpcore.object.ModifierType;
import net.xelcore.rpcore.object.RpPlayer;
import net.xelcore.rplib.message.Message;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class EconomyCommand implements CommandExecutor {


    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player p = (Player) sender;
            RpPlayer rp = RpPlayer.getPlayer(p);
            Economy e = new Economy(rp);
            if (args.length == 0) {
                new Message(p).prefix("Du besitzt aktuell §6" + rp.getGold() + "g");
            } else {
                if (args[0].equalsIgnoreCase("pay")) {
                    if (args.length == 3) {
                        Player target = Bukkit.getPlayer(args[1]);
                        if (target != null && target.isOnline()) {
                            if(!target.getName().equals(p.getName())) {
                                if (Integer.valueOf(args[2]) instanceof Integer) {
                                    int amount = Integer.parseInt(args[2]);
                                    if (rp.getGold() >= amount) {
                                        e.transfer(RpPlayer.getPlayer(target), amount);
                                        new Message(p).prefix("Du hast " + target.getName() + " §6" + amount + "g §7gegeben");
                                        new Message(target).prefix("§7" + p.getName() + " hat dir §6" + amount + "g §7gegeben");
                                    } else {
                                        new Message(p).errorprefix("Du hast nicht genug Gold");
                                    }
                                } else {
                                    new Message(p).wrongcmdusage("/gold pay [Spieler] [Betrag]");
                                }
                            } else {
                                new Message(p).errorprefix("Du kannst dir selbst kein Gold geben");
                            }
                        } else {
                            new Message(p).notonline();
                        }
                    } else {
                        new Message(p).wrongcmdusage("/gold pay [Spieler] [Betrag]");
                    }
                } else if (args[0].equalsIgnoreCase("admin")) {
                    if(p.hasPermission("rp.core.economy.admin")) {
                        if (args.length == 4) {
                            Player target = Bukkit.getPlayer(args[2]);
                            if (target != null && target.isOnline()) {
                                RpPlayer rpt = RpPlayer.getPlayer(target);
                                if (Integer.valueOf(args[3]) instanceof Integer) {
                                    int amount = Integer.parseInt(args[3]);
                                    if (args[1].equalsIgnoreCase("set")) {
                                        rpt.setGold(amount);
                                        new Message(target).prefix("§7Du hast jetzt §6" + amount + "g");
                                        new Message(p).prefix("§7" + target.getName() + " hat jetzt §6" + amount + "g");
                                    } else if (args[1].equalsIgnoreCase("add")) {
                                        rpt.addGold(amount);
                                        new Message(target).prefix("§7Du hast §6" + amount + "g §7erhalten");
                                        new Message(p).prefix("§7" + target.getName() + " hat §6" + amount + "g §7erhalten");
                                    } else if (args[1].equalsIgnoreCase("remove")) {
                                        if (!(rpt.getGold() < amount)) {
                                            rpt.removeGold(amount);
                                            new Message(target).prefix("§7Du hast §6" + amount + "g §7verloren");
                                            new Message(p).prefix("§7" + target.getName() + " hat §6" + amount + "g §7verloren");
                                        } else {
                                            new Message(p).errorprefix("§7" + target.getName() + " hat weniger als §6" + amount + "g");
                                        }
                                    } else {
                                        new Message(p).wrongcmdusage("/gold admin <get/set/add/remove> [Spieler] <Betrag>");
                                    }
                                } else {
                                    new Message(p).wrongcmdusage("/gold admin <get/set/add/remove> [Spieler] <Betrag>");
                                }
                            } else {
                                new Message(p).notonline();
                            }
                        } else if (args.length == 3) {
                            Player target = Bukkit.getPlayer(args[2]);
                            if (target.isOnline()) {
                                RpPlayer rpt = RpPlayer.getPlayer(target);
                                if (args[1].equalsIgnoreCase("get")) {
                                    new Message(p).prefix("§7" + target.getName() + " hat §6" + rpt.getGold() + "g");
                                } else {
                                    new Message(p).wrongcmdusage("/gold admin <get/set/add/remove> [Spieler] <Betrag>");
                                }
                            } else {
                                new Message(p).notonline();
                            }
                        } else {
                            new Message(p).wrongcmdusage("/gold admin <get/set/add/remove> [Spieler] <Betrag>");
                        }
                    } else {
                        new Message(p).noperms();
                    }
                } else {
                    new Message(p).wrongcmdusage("/gold pay [Spieler] [Betrag]");
                }
            }
        } else {
            Main.log.error("Only players can execute this command");
        }
        return true;
    }
}
