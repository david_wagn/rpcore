package net.xelcore.rpcore.command;

import net.xelcore.rpcore.inventory.ProfileInventory;
import net.xelcore.rpcore.main.Main;
import net.xelcore.rplib.message.Message;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ProfileCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(sender instanceof Player) {
            Player p = (Player) sender;
            if(args.length == 0) {
                new ProfileInventory(p, p);
            } else if(args.length == 1) {
                Player target = Bukkit.getPlayer(String.valueOf(args[0]));
                if(target != null) {
                    new ProfileInventory(p, target);
                } else {
                    new Message(p).notonline();
                    return true;
                }
            } else {
                new Message(p).wrongcmdusage("/profile <Spieler>");
                return true;
            }
        } else {
            Main.log.error("Only players can execute this command");
            return true;
        }
        return true;
    }
}
