package net.xelcore.rpcore.command;

import net.xelcore.rpcore.inventory.SkillInventory;
import net.xelcore.rpcore.main.Main;
import net.xelcore.rplib.message.Message;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SkillCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(sender instanceof Player) {
            Player p = (Player) sender;
            if(args.length == 0) {
                new SkillInventory(p);
            } else {
                new Message(p).wrongcmdusage("/skills");
            }
        } else {
            Main.log.error("Only players can execute this command");
            return true;
        }
        return true;
    }
}
