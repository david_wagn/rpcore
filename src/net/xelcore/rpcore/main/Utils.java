package net.xelcore.rpcore.main;

import net.coreprotect.CoreProtectAPI;
import net.xelcore.rpcore.object.Attribute;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

public class Utils {

    public static double format(double d) {
        return Math.round(d * 10.0) / 10.0;
    }

    public static String uuid(Player player) {
        return player.getUniqueId().toString();
    }

    public static boolean placedByPlayer(Block b) {
        CoreProtectAPI api = new CoreProtectAPI();
        List<String[]> result = api.blockLookup(b, 0);
        for(String[] s : result) {
            CoreProtectAPI.ParseResult r = api.parseResult(s);
            if(r.getActionId() == 1) {
                return true;
            }
        }
        return false;
    }

    public static int randomNumberRange(int min, int max) {

        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }

    public static String formatAttribute(Attribute attribute) {
        String format = "";
        switch (attribute) {
            case STRENGTH:
                format = "§cStärke";
                break;
            case AGILITY:
                format = "§aAgilität";
                break;
            case INTELLIGENCE:
                format = "§bIntelligenz";
                break;

        }
        return format;
    }

    public static boolean chance(Integer percantage) {
        return Math.random() <= (Double.valueOf(percantage)/100.0);
    }

    public static Double[] castToDoubleArray(String[] array) {
        Double[] d = {};
        for (int i = 0; i < array.length; i++) {
            d[i] = Double.valueOf(array[i]);
        }
        return d;
    }

    public static List<Block> getNearbyBlocks(Location location, int radius) {
        List<Block> blocks = new ArrayList<>();
        for(int x = location.getBlockX() - radius; x <= location.getBlockX() + radius; x++) {
            for(int y = location.getBlockY() - radius; y <= location.getBlockY() + radius; y++) {
                for(int z = location.getBlockZ() - radius; z <= location.getBlockZ() + radius; z++) {
                    blocks.add(location.getWorld().getBlockAt(x, y, z));
                }
            }
        }
        return blocks;
    }

    public static Player getNearestPlayerInSight(Player player, int range) {
        ArrayList<Entity> entities = (ArrayList<Entity>) player.getNearbyEntities(range, range, range);
        ArrayList<Block> sightBlock = (ArrayList<Block>) player.getLineOfSight(null, range);
        ArrayList<Location> sight = new ArrayList<>();
        for (Block block : sightBlock) sight.add(block.getLocation());
        for (Location location : sight) {
            for (Entity entity : entities) {
                if (Math.abs(entity.getLocation().getX() - location.getX()) < 1.3) {
                    if (Math.abs(entity.getLocation().getY() - location.getY()) < 1.5) {
                        if (Math.abs(entity.getLocation().getZ() - location.getZ()) < 1.3) {
                            if (entity instanceof Player) {
                                return (Player) entity;
                            } else {
                                return null;
                            }
                        }
                    }
                }
            }
        }
        return null;
    }

    public static Entity getNearestEntityInSight(Player player, int range) {
        ArrayList<Entity> entities = (ArrayList<Entity>) player.getNearbyEntities(range, range, range);
        ArrayList<Block> sightBlock = (ArrayList<Block>) player.getLineOfSight(null, range);
        ArrayList<Location> sight = new ArrayList<>();
        for (Block block : sightBlock) sight.add(block.getLocation());
        for (Location location : sight) {
            for (Entity entity : entities) {
                if (Math.abs(entity.getLocation().getX() - location.getX()) < 1.3) {
                    if (Math.abs(entity.getLocation().getY() - location.getY()) < 1.5) {
                        if (Math.abs(entity.getLocation().getZ() - location.getZ()) < 1.3) {
                            return entity;
                        }
                    }
                }
            }
        }
        return null;
    }

    public static Entity[] getNearbyEntities(Location l, int radius) {
        int chunkRadius = radius < 16 ? 1 : (radius - (radius % 16)) / 16;
        HashSet<Entity> radiusEntities = new HashSet<>();
        for (int chX = -chunkRadius; chX <= chunkRadius; chX++) {
            for (int chZ = -chunkRadius; chZ <= chunkRadius; chZ++) {
                int x = (int) l.getX(), y = (int) l.getY(), z = (int) l.getZ();
                for (Entity e : new Location(l.getWorld(), x + (chX * 16), y, z
                        + (chZ * 16)).getChunk().getEntities()) {
                    if (e.getLocation().distance(l) <= radius
                            && e.getLocation().getBlock() != l.getBlock()) {
                        radiusEntities.add(e);
                    }
                }
            }
        }
        return radiusEntities.toArray(new Entity[radiusEntities.size()]);
    }

}
