package net.xelcore.rpcore.main;

import net.xelcore.rpcore.ability.PassiveExecutor;
import net.xelcore.rpcore.command.*;
import net.xelcore.rpcore.data.*;
import net.xelcore.rpcore.listener.*;
import net.xelcore.rpcore.object.PlayerSkills;
import net.xelcore.rpcore.object.RpPlayer;
import net.xelcore.rplib.inventory.InventoryBuilder;
import net.xelcore.rplib.logger.Logger;
import net.xelcore.rplib.message.Message;
import net.xelcore.rplib.message.Title;
import net.xelcore.rplib.sql.MySQL;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.sql.Connection;
import java.util.concurrent.TimeUnit;

public class Main extends JavaPlugin {

    public static Logger log;
    public static Connection connection;
    public static Main instance;
    public static Integer onlineTime;
    public static boolean limitTime;

    @Override
    public void onEnable() {

        /* Setting default values */
        log = net.xelcore.rplib.main.Main.log;
        log.toggleBroadcast(); // Should only be enabled during development
        instance = this;
        onlineTime = 70; // Playtime limit in seconds
        limitTime = false;

        /* Setting api instances */
        InventoryBuilder.setPlugin(this);


        /* Connecting to database */
        MySQLData.loadConfig();
        connection = new MySQL(
                MySQLData.getConfig().getString("mysql.hostname"),
                MySQLData.getConfig().getString("mysql.database"),
                MySQLData.getConfig().getString("mysql.username"),
                MySQLData.getConfig().getString("mysql.password"),
                MySQLData.getConfig().getInt("mysql.port")).connect();

        /* Initializing data loading from database for abilities, classes and jobs */
        AbilityData.init();
        ClassData.init();
        JobData.init();


        /* General listeners */
        Bukkit.getPluginManager().registerEvents(new PlayerJoinListener(), this);
        Bukkit.getPluginManager().registerEvents(new PlayerQuitListener(), this);
        Bukkit.getPluginManager().registerEvents(new PlayerDeathListener(), this);
        Bukkit.getPluginManager().registerEvents(new PlayerChatListener(), this);
        Bukkit.getPluginManager().registerEvents(new PlayerRespawnListener(), this);
        Bukkit.getPluginManager().registerEvents(new PlayerDamageListener(), this);
        Bukkit.getPluginManager().registerEvents(new PlayerExpChangeListener(), this);
        Bukkit.getPluginManager().registerEvents(new PlayerLevelupListener(), this);
        Bukkit.getPluginManager().registerEvents(new ServerListPingListener(), this);

        /* Ability listeners */
        Bukkit.getPluginManager().registerEvents(new PlayerSwapItemsListener(), this);
        Bukkit.getPluginManager().registerEvents(new PlayerItemHeldListener(), this);
        Bukkit.getPluginManager().registerEvents(new PlayerToggleAbilitySelectionListener(), this);
        Bukkit.getPluginManager().registerEvents(new PassiveExecutor(), this);
        Bukkit.getPluginManager().registerEvents(new PlayerMoveListener(), this);

        /* Job listeners */
        Bukkit.getPluginManager().registerEvents(new JobListener(), this);

        /* Gold/Economy listener */
        Bukkit.getPluginManager().registerEvents(new PlayerPickupItemListener(), this);
        Bukkit.getPluginManager().registerEvents(new EntityDropItemListener(), this);
        Bukkit.getPluginManager().registerEvents(new ItemMergeListener(), this);
        Bukkit.getPluginManager().registerEvents(new EntityKillListener(), this);
        Bukkit.getPluginManager().registerEvents(new EntitySpawnListener(), this);

        /* Registering commands */
        this.getCommand("profile").setExecutor(new ProfileCommand());
        this.getCommand("class").setExecutor(new ClassCommand());
        this.getCommand("skills").setExecutor(new SkillCommand());
        this.getCommand("timelimit").setExecutor(new TimeLimitCommand());
        this.getCommand("playtime").setExecutor(new PlaytimeCommand());
        this.getCommand("economy").setExecutor(new EconomyCommand());

        /* Playtime listener, do not modify any of the timelimit data */
        Bukkit.getScheduler().scheduleSyncRepeatingTask(instance, () -> {
            if(limitTime) {
                for (Player all : Bukkit.getOnlinePlayers()) {
                    if(!all.hasPermission("rp.core.bypass.timelimit")) {
                        RpPlayer rp = RpPlayer.getPlayer(all);
                        if (System.currentTimeMillis() / 1000 == rp.getJoined() + onlineTime) {
                            all.kickPlayer("§7Deine Zeit ist abgelaufen");
                        } else if (System.currentTimeMillis() / 1000 == rp.getJoined() + onlineTime - 1) {
                            new Message(all).prefix("Du wirst in §61 Sekunde §7gekickt");
                        } else if (System.currentTimeMillis() / 1000 == rp.getJoined() + onlineTime - 2) {
                            new Message(all).prefix("Du wirst in §62 Sekunden §7gekickt");
                        } else if (System.currentTimeMillis() / 1000 == rp.getJoined() + onlineTime - 3) {
                            new Message(all).prefix("Du wirst in §63 Sekunden §7gekickt");
                        } else if (System.currentTimeMillis() / 1000 == rp.getJoined() + onlineTime - 4) {
                            new Message(all).prefix("Du wirst in §64 Sekunden §7gekickt");
                        } else if (System.currentTimeMillis() / 1000 == rp.getJoined() + onlineTime - 5) {
                            new Message(all).prefix("Du wirst in §65 Sekunden §7gekickt");
                        } else if (System.currentTimeMillis() / 1000 == rp.getJoined() + onlineTime - 10) {
                            new Message(all).prefix("Du wirst in §610 Sekunden §7gekickt");
                        } else if (System.currentTimeMillis() / 1000 == rp.getJoined() + onlineTime - 30) {
                            new Message(all).prefix("Du wirst in §630 Sekunden §7gekickt");
                        } else if (System.currentTimeMillis() / 1000 == rp.getJoined() + onlineTime - 60) {
                            new Title(all, "§4Achtung!", "§7Du wirst in §660 Sekunden §7gekickt", 10, 30, 10, 10, 30, 10);
                        }
                    }
                }
            }
        }, 20, 20);
    }

    @Override
    public void onDisable() {
        /* Crashhandler for sudden stop/restart/reload */
        log.debug("rpcore>CrashHandler", "Saving of playerdata due to server reload...");
        for (Player all : Bukkit.getOnlinePlayers()) {
            RpPlayer rp = RpPlayer.getPlayer(all);
            PlayerSkills ps = new PlayerSkills(rp);
            PlayerJobData pjd = new PlayerJobData(all);
            EconomyData ed = new EconomyData(all);
            ps.save();
            log.debug("rpcore>CrashHandler", all.getName() + " skilldata save complete");
            rp.save();
            log.debug("rpcore>CrashHandler", all.getName() + " playerdata save complete");
            pjd.save(rp);
            log.debug("rpcore>CrashHandler", all.getName() + " jobdata save complete");
            ed.save(rp);
            log.debug("rpcore>CrashHandler", all.getName() + " economydata save complete");
        }
        log.debug("rpcore>CrashHandler", "Saving complete!");
        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        for (Player all : Bukkit.getOnlinePlayers()) {
            all.kickPlayer("§6Server is reloading");
        }
    }

}
