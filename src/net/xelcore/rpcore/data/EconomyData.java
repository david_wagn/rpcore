package net.xelcore.rpcore.data;

import net.xelcore.rpcore.main.Main;
import net.xelcore.rpcore.object.RpPlayer;
import org.bukkit.entity.Player;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;

public class EconomyData {

    private Player player;
    private String uuid;
    private Integer gold;

    public EconomyData(Player p) {
        this.player = p;
        this.uuid = p.getUniqueId().toString();
    }

    public boolean exists() {
        try {
            final ResultSet rs = MySQLData.count("economy", Arrays.asList("uuid", "=", this.uuid));
            if (rs.next()) {
                return rs.getInt(1) > 0;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public void create() {
        try {
            Main.connection.prepareStatement("INSERT INTO `economy`(`uuid`, `value`) VALUES ('" + this.uuid + "', '0')").execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void save(RpPlayer player) {
        try {
            Main.connection.prepareStatement("UPDATE economy SET value='" + player.getGold() + "' WHERE uuid='" + player.getPlayer().getUniqueId().toString() + "'").execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public EconomyData getValues() {
        try {
            final ResultSet rs = MySQLData.get("economy", Arrays.asList("uuid", "=", this.uuid));
            while(rs.next()) {
                this.gold =  rs.getInt("value");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return this;
    }

    public Player getPlayer() {
        return this.player;
    }

    public String getUUID() {
        return uuid;
    }

    public Integer getGold() {
        return gold;
    }

}
