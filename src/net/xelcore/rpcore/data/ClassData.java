package net.xelcore.rpcore.data;

import net.xelcore.rpcore.main.Main;
import net.xelcore.rpcore.object.Ability;
import net.xelcore.rpcore.object.Attribute;
import net.xelcore.rpcore.object.BaseClass;
import net.xelcore.rpcore.object.RpClass;
import org.bukkit.Material;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ClassData {

    public static void init() {
        Main.log.info(" ");
        Main.log.info("----------[Classloader]----------");
        Main.log.info("Loading Baseclasses...");
        loadBaseClasses();
        Main.log.info("Loaded " + BaseClass.classList.size() + " baseclasses");
        Main.log.info(" ");
        Main.log.info("Loading Classes...");
        loadClasses();
        Main.log.info("Loaded " + RpClass.classList.size() + " classes");
        Main.log.info("----------[Classloader]----------");
        Main.log.info(" ");
    }

    private static void loadBaseClasses() {
        try {
            final ResultSet rs = MySQLData.get("baseclasses", null);
            while(rs.next()) {
                BaseClass bc = new BaseClass(rs.getInt("id"), rs.getString("name"), Attribute.valueOf(rs.getString("primaryAttribute")), rs.getString("colorCode"), Material.valueOf(rs.getString("material")));
                Main.log.info("Loaded baseclass " + bc.getName() + " (" + bc.getId() + ") with primaryAttribute " + bc.getPrimaryAttribute().toString());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void loadClasses() {
        try {
            final ResultSet rs = MySQLData.get("classes", null);
            while (rs.next()) {
                List<Double> baseAttributes = Arrays.asList(Double.valueOf(rs.getString("baseAttributes").split(";")[0]), Double.valueOf(rs.getString("baseAttributes").split(";")[1]), Double.valueOf(rs.getString("baseAttributes").split(";")[2]));
                List<Double> attributeGain = Arrays.asList(Double.valueOf(rs.getString("attributeGain").split(";")[0]), Double.valueOf(rs.getString("attributeGain").split(";")[1]), Double.valueOf(rs.getString("attributeGain").split(";")[2]));
                String[] a = rs.getString("abilities").split(",");
                ArrayList<Ability> abilities = new ArrayList<>();
                for (String s : a) {
                    if (Ability.abilities.get(Integer.valueOf(s)) != null) {
                        abilities.add(Ability.abilities.get(Integer.valueOf(s)));
                    } else {
                        Main.log.warn("Ability with id " + Integer.valueOf(s) + " doesn't exist, ignoring");
                    }
                }
                RpClass rc = new RpClass(rs.getInt("id"), BaseClass.classList.get(rs.getInt("baseClass")), rs.getString("name"), rs.getString("description"), rs.getString("colorCode"), Material.valueOf(rs.getString("material")), baseAttributes, attributeGain, abilities);
                Main.log.info("Loaded class " + rc.getName() + " (" + rc.getId() + ") with baseclass " + BaseClass.classList.get(rs.getInt("baseClass")).getName() + " and " + rc.getAbilities().size() + " " + (rc.getAbilities().size() == 1 ? "ability" : "abilities"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
