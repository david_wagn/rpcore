package net.xelcore.rpcore.data;

import net.xelcore.rpcore.main.Main;
import net.xelcore.rpcore.main.Utils;
import net.xelcore.rpcore.object.RpClass;
import net.xelcore.rpcore.object.RpPlayer;
import org.bukkit.entity.Player;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

public class PlayerData {

    private String uuid;
    private Player player;
    private List<Double> attributes;
    private Integer skillPoints;
    private Integer classPoints;
    private Integer level;
    private Integer xp;
    private RpClass rpClass;

    public PlayerData(Player p) {
        this.uuid = p.getUniqueId().toString();
        this.player = p;
    }

    public boolean exists() {
        try {
            final ResultSet rs = MySQLData.count("players", Arrays.asList("uuid", "=", this.uuid));
            if (rs.next()) {
                return rs.getInt(1) > 0;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public void create() {
        try {
            Main.connection.prepareStatement("INSERT INTO `players`(`uuid`, `attributes`, `skillpoints`, `classpoints`, `level`, `xp`, `class`) VALUES ('" + this.uuid + "', '5.0;5.0;5.0', '0', '0', '0', '0', '0')").execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void save(RpPlayer player) {
        try {
            Main.connection.prepareStatement("UPDATE players SET attributes='" + Utils.format(player.getStrength()) + ";" + Utils.format(player.getAgility()) + ";"+ Utils.format(player.getIntelligence()) + "', skillpoints='" + player.getSkillPoints() + "', classpoints='" + player.getClassPoints() + "', level='" + player.getLevel() + "', xp='" + player.getXp() + "', class='" + player.getRpClass().getId() + "' WHERE uuid='" + player.getPlayer().getUniqueId().toString() + "'").execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public PlayerData getValues() {
        try {
            final ResultSet rs = MySQLData.get("players", Arrays.asList("uuid", "=", this.uuid));
            while(rs.next()) {
                String[] values = rs.getString("attributes").split(";");
                this.attributes =  Arrays.asList(Double.valueOf(values[0]), Double.valueOf(values[1]), Double.valueOf(values[2]));
                this.skillPoints = rs.getInt("skillpoints");
                this.classPoints = rs.getInt("classpoints");
                this.level = rs.getInt("level");
                this.xp = rs.getInt("xp");
                this.rpClass =  RpClass.classList.get(rs.getInt("class"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return this;
    }

    public double getStrength() {
        return getAttributes().get(0);
    }

    public double getAgility() {
        return getAttributes().get(1);
    }

    public double getIntelligence() {
        return getAttributes().get(2);
    }

    public RpClass getRpClass() {
        return rpClass;
    }

    public Integer getXp() {
        return xp;
    }

    public Integer getLevel() {
        return level;
    }

    public Integer getClassPoints() {
        return classPoints;
    }

    public Integer getSkillPoints() {
        return skillPoints;
    }

    public List<Double> getAttributes() {
        return attributes;
    }

    public Player getPlayer() {
        return player;
    }
}
