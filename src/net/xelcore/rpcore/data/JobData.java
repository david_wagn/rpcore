package net.xelcore.rpcore.data;

import net.xelcore.rpcore.main.Main;
import net.xelcore.rpcore.object.Job;
import net.xelcore.rpcore.object.JobType;
import org.bukkit.Material;

import java.sql.ResultSet;
import java.sql.SQLException;

public class JobData {

    public static void init() {
        Main.log.info(" ");
        Main.log.info("----------[Lobloader]----------");
        Main.log.info("Loading Jobs...");
        loadJobs();
        Main.log.info("Loaded " + Job.jobList.size() + " jobs");
        Main.log.info("----------[Lobloader]----------");
        Main.log.info(" ");
    }

    private static void loadJobs() {
        try {
            final ResultSet rs = MySQLData.get("jobs", null);
            while (rs.next()) {
                Job j = new Job(rs.getInt("id"), rs.getString("name"), rs.getString("description"), Material.valueOf(rs.getString("material")), JobType.valueOf(rs.getString("type")), rs.getInt("basexp"), rs.getDouble("xpmultiplier"));
                Main.log.info("Loaded job " + j.getName() + " (" + j.getId() + ")");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
