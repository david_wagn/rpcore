package net.xelcore.rpcore.data;

import net.xelcore.rpcore.main.Main;
import net.xelcore.rpcore.object.*;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AbilityData {

    public static void init() {
        Main.log.info(" ");
        Main.log.info("----------[Abilityloader]----------");
        Main.log.info("Loading Abilities...");
        loadAbilities();
        Main.log.info("Loaded " + Ability.abilities.size() + " abilities");
        Main.log.info("----------[Classloader]----------");
        Main.log.info(" ");
    }

    private static void loadAbilities() {
        try {
            final ResultSet rs = MySQLData.get("abilities", null);
            while(rs.next()) {
                Ability a = new Ability(rs.getInt("id"), rs.getString("name"), AbilityType.valueOf(rs.getString("abilityType")), CastType.valueOf(rs.getString("castType")), rs.getString("description"), rs.getDouble("levelAmp"), rs.getString("value"), rs.getDouble("duration"), rs.getDouble("cooldown"), rs.getDouble("manacost"));
                Main.log.info("Loaded ability " + a.getName() + " (" + a.getId() + ") with abilityType " + a.getAbilityType().toString() + " and castType " + a.getCastType().toString());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
