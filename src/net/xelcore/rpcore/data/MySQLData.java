package net.xelcore.rpcore.data;

import net.xelcore.rpcore.main.Main;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class MySQLData {

    public static void loadConfig() {
        File file = new File(Bukkit.getWorldContainer() + "/rpdata", "mysql.yml");
        if(!file.exists()) {
            try {
                if(!file.getParentFile().exists()) {
                    file.getParentFile().mkdir();
                }
                file.createNewFile();
                FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);
                cfg.set("mysql.hostname", "hostname");
                cfg.set("mysql.port", "port");
                cfg.set("mysql.username", "username");
                cfg.set("mysql.password", "password");
                cfg.set("mysql.database", "database");
                cfg.save(file);

            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }

    public static FileConfiguration getConfig() {
        File file = new File(Bukkit.getWorldContainer() + "/rpdata", "mysql.yml");
        return YamlConfiguration.loadConfiguration(file);
    }

    public static ResultSet count(String table, List<String> selector) {
        ResultSet rs;
        try {
            if (selector == null)
                return Main.connection.prepareStatement("SELECT count(*) FROM " + table).executeQuery();
            else if (!(selector.size() > 3))
                return Main.connection.prepareStatement("SELECT count(*) FROM " + table + " WHERE " + selector.get(0) + "" +  selector.get(1) + "'" + selector.get(2) + "'").executeQuery();
            else
                Main.log.error("Cant parse more than 3 selector modifiers");
            return null;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static ResultSet get(String table, List<String> selector) {
        ResultSet rs;
        try {
            if (selector == null)
                return Main.connection.prepareStatement("SELECT * FROM " + table).executeQuery();
            else if (!(selector.size() > 3))
                return Main.connection.prepareStatement("SELECT * FROM " + table + " WHERE " + selector.get(0) + selector.get(1) + "'" + selector.get(2) + "'").executeQuery();
            else
                Main.log.error("Cant parse more than 3 selector modifiers");
            return null;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
