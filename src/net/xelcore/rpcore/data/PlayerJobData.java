package net.xelcore.rpcore.data;

import net.xelcore.rpcore.main.Main;
import net.xelcore.rpcore.main.Utils;
import net.xelcore.rpcore.object.*;
import org.bukkit.entity.Player;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;

public class PlayerJobData {

    private Player player;
    private RpPlayer rpPlayer;
    private String uuid;
    private HashMap<Job, Integer> jobLevel = new HashMap<>();
    private HashMap<Job, Integer> jobXp = new HashMap<>();

    public PlayerJobData(Player p) {
        this.player = p;
        this.rpPlayer = RpPlayer.getPlayer(p);
        this.uuid = p.getUniqueId().toString();
    }

    public void save(RpPlayer rp) {
        for (PlayerJob j : rp.getJobs().values()) {
            try {
                Main.connection.prepareStatement("UPDATE playerjobs SET level='" + j.getLevel() + "', xp='" + j.getXp() + "' WHERE uuid='" + Utils.uuid(rp.getPlayer()) + "' AND jobid='" + j.getJob().getId() + "'").execute();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public PlayerJobData getValues() {
        try {
            final ResultSet rs = MySQLData.get("playerjobs", Arrays.asList("uuid", "=", this.uuid));
            while(rs.next()) {
                if(rs.getString("uuid").equals(this.uuid)) {
                    jobLevel.put(Job.jobList.get(rs.getInt("jobid")), rs.getInt("level"));
                    jobXp.put(Job.jobList.get(rs.getInt("jobid")), rs.getInt("xp"));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return this;
    }

    public boolean exists() {
        try {
            final ResultSet rs = MySQLData.count("playerjobs", Arrays.asList("uuid", "=", this.uuid));
            if (rs.next()) {
                return rs.getInt(1) > 0;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public void create() {
        for (Job j : Job.jobList.values()) {
            try {
                Main.connection.prepareStatement("INSERT INTO playerjobs(uuid, jobid, level, xp) VALUES ('" + this.uuid + "', '" + j.getId() + "', '0', '0')").execute();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public Player getPlayer() {
        return player;
    }

    public RpPlayer getRpPlayer() {
        return rpPlayer;
    }

    public Integer getJobXp(Job j) {
        return jobXp.get(j);
    }

    public Integer getJobLevel(Job j) {
        return jobLevel.get(j);
    }

    public HashMap<Job, Integer> getJobLevelList() {
        return jobLevel;
    }

    public HashMap<Job, Integer> getJobXpList() {
        return jobXp;
    }
}
