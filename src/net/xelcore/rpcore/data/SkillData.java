package net.xelcore.rpcore.data;

import net.xelcore.rpcore.main.Main;
import net.xelcore.rpcore.main.Utils;
import net.xelcore.rpcore.object.Ability;
import net.xelcore.rpcore.object.RpClass;
import net.xelcore.rpcore.object.RpPlayer;
import org.bukkit.entity.Player;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;

public class SkillData {

    private Player player;
    private RpPlayer rpPlayer;
    private String uuid;
    private HashMap<Ability, Integer> skills = new HashMap<>();

    public SkillData(Player p) {
        this.player = p;
        this.rpPlayer = RpPlayer.getPlayer(p);
        this.uuid = p.getUniqueId().toString();
    }

    public boolean exists() {
        try {
            final ResultSet rs = MySQLData.count("skills", Arrays.asList("uuid", "=", this.uuid));
            if (rs.next()) {
                return rs.getInt(1) > 0;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void save(RpPlayer rp) {
        for(Ability a : rp.getRpClass().getAbilities()) {
            if(rp.getSkills().containsKey(a)) {
                try {
                    Main.connection.prepareStatement("UPDATE skills SET level='" + rp.getSkills().get(a) + "' WHERE uuid='" + Utils.uuid(rp.getPlayer()) + "' AND abilityid='" + a.getId() + "'").execute();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void create() {
        if(this.rpPlayer.getRpClass() != RpClass.classList.get(0)) {
            for(Ability a : rpPlayer.getRpClass().getAbilities()) {
                try {
                    Main.connection.prepareStatement("INSERT INTO skills(uuid, abilityid, level) VALUES ('" + this.uuid + "', '" + a.getId() + "', '0')").execute();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public SkillData getValues() {
        try {
            final ResultSet rs = MySQLData.get("skills", Arrays.asList("uuid", "=", this.uuid));
            while(rs.next()) {
                if(rs.getString("uuid").equals(this.uuid)) {
                    skills.put(Ability.abilities.get(rs.getInt("abilityid")), rs.getInt("level"));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return this;
    }

    public HashMap<Ability, Integer> getSkills() {
        return skills;
    }

    public Player getPlayer() {
        return player;
    }

    public RpPlayer getRpPlayer() {
        return rpPlayer;
    }
}
