package net.xelcore.rpcore.listener;

import net.xelcore.rpcore.main.Main;
import net.xelcore.rpcore.object.RpPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

public class PlayerDeathListener implements Listener {

    @EventHandler
    public void death(PlayerDeathEvent e) {
        Player p = e.getEntity();
        RpPlayer rp = RpPlayer.getPlayerList().get(p.getUniqueId().toString());
        Main.log.debug("rpcore>PlayerInterval", "Player " + p.getName() + " died, stopping async thread");
        rp.getTimer().cancel();
    }
}
