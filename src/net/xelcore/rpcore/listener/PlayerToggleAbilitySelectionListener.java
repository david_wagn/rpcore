package net.xelcore.rpcore.listener;

import net.xelcore.rpcore.event.PlayerToggleAbilitySelectionEvent;
import net.xelcore.rpcore.main.Main;
import net.xelcore.rpcore.object.RpPlayer;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.util.Arrays;

public class PlayerToggleAbilitySelectionListener implements Listener {

    @EventHandler
    public void toggleabilityselection(PlayerToggleAbilitySelectionEvent e) {
        Player p = e.getPlayer();
        RpPlayer rp = e.getRpPlayer();

        rp.setCastingAbility(!rp.isCastingAbility());
        rp.setCastSlot(e.getSelectionSlot());
        if (rp.isCastingAbility()) {
            p.playSound(p.getLocation(), Sound.BLOCK_BEACON_ACTIVATE, 1, 1);
            p.playSound(p.getLocation(), Sound.UI_TOAST_IN, 1, 1);
        } else {
            p.playSound(p.getLocation(), Sound.UI_TOAST_OUT, 1, 1);
        }


        if (rp.isCastingAbility()) {
            Integer[] slots = {0, 1, 2, 3, 4, 5, 6, 7, 8};
            for (int i = 0; i < slots.length; i++) {
                if (slots[i].equals(rp.getCastSlot())) {
                    System.arraycopy(slots, i + 1, slots, i, slots.length - 1 - i);
                    break;
                }
            }
            rp.setCastSlots(Arrays.copyOf(slots, 5));
        } else {
            rp.setCastSlot(null);
        }
        if (rp.isCastingAbility()) Main.log.debug("rpcore>AbilityCaster", "" + p.getName() + " is selecting ability");
        else Main.log.debug("rpcore>AbilityCaster", "" + p.getName() + " stopped selection");
    }
}
