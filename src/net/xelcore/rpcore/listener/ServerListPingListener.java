package net.xelcore.rpcore.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerListPingEvent;

public class ServerListPingListener implements Listener {

    @EventHandler
    public void serverListPing(ServerListPingEvent e) {
        e.setMotd("§6Roleplay Server §av1.2§7, hosted by §3Xelcore\n§7Infos & Wiki unter §dhttps.//dwagner.one/rp");
    }
}
