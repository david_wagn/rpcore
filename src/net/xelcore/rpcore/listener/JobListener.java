package net.xelcore.rpcore.listener;

import net.xelcore.rpcore.main.Utils;
import net.xelcore.rpcore.object.Job;
import net.xelcore.rpcore.object.JobType;
import net.xelcore.rpcore.object.PlayerJob;
import net.xelcore.rpcore.object.RpPlayer;
import net.xelcore.rplib.message.Title;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.SoundCategory;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.player.PlayerFishEvent;

public class JobListener implements Listener {

    /*
    * BlockBreakEvent: Miner, Lumberjack
    * */

    @EventHandler
    public void blockbreak(BlockBreakEvent e) {
        Player p = e.getPlayer();
        RpPlayer rp = RpPlayer.getPlayer(p);
        int amount = e.getExpToDrop();
        if(!Utils.placedByPlayer(e.getBlock())) {
            if(e.getBlock().getType() == Material.EMERALD_ORE ||
                    e.getBlock().getType() == Material.DIAMOND_ORE ||
                    e.getBlock().getType() == Material.REDSTONE_ORE ||
                    e.getBlock().getType() == Material.GOLD_ORE ||
                    e.getBlock().getType() == Material.LAPIS_ORE ||
                    e.getBlock().getType() == Material.IRON_ORE ||
                    e.getBlock().getType() == Material.COAL_ORE ||
                    e.getBlock().getType() == Material.NETHER_QUARTZ_ORE) {
                PlayerJob pj = rp.getJobs().get(Job.fromType(JobType.MINER));
                amount+=(amount*(Job.fromType(JobType.MINER).getXpGain()*pj.getLevel()));
                if(pj.getXp() > pj.getXpNeeded() || pj.getXp()+amount > pj.getXpNeeded()) {
                    pj.setLevel(pj.getLevel()+1);
                    pj.setXp(0);
                    p.playSound(p.getLocation(), Sound.UI_TOAST_CHALLENGE_COMPLETE, SoundCategory.MASTER, 1, 1);
                    new Title(p, "§6Mining levelup", "§7Du bist jetzt Level §6" + pj.getLevel() + " §7Miner", 30, 80, 30, 30, 80, 30);
                } else {
                    pj.setXp(pj.getXp()+amount);
                }
                e.setExpToDrop(amount);
            } else if(e.getBlock().getType() == Material.OAK_LOG || e.getBlock().getType() == Material.BIRCH_LOG || e.getBlock().getType() == Material.SPRUCE_LOG || e.getBlock().getType() == Material.DARK_OAK_LOG || e.getBlock().getType() == Material.ACACIA_LOG) {
                PlayerJob pj = rp.getJobs().get(Job.fromType(JobType.LUMBERJACK));
                amount = Utils.randomNumberRange(1, 5);
                amount+=(amount*(Job.fromType(JobType.LUMBERJACK).getXpGain()*pj.getLevel()));
                if(pj.getXp() > pj.getXpNeeded() || pj.getXp()+amount > pj.getXpNeeded()) {
                    pj.setLevel(pj.getLevel()+1);
                    pj.setXp(0);
                    p.playSound(p.getLocation(), Sound.UI_TOAST_CHALLENGE_COMPLETE, SoundCategory.MASTER, 1, 1);
                    new Title(p, "§6Lumberjack levelup", "§7Du bist jetzt Level §6" + pj.getLevel() + " §7Lumberjack", 30, 80, 30, 30, 80, 30);
                } else {
                    pj.setXp(pj.getXp()+amount);
                }
                e.setExpToDrop(amount);
            }
        }
    }

    /*
    * Event: Hunter
    * */
    @EventHandler
    public void kill(EntityDeathEvent e) {
        if(e.getEntity().getKiller() instanceof Player) {
            Player p = e.getEntity().getKiller();
            RpPlayer rp = RpPlayer.getPlayer(p);

            int amount = e.getDroppedExp();
            PlayerJob pj = rp.getJobs().get(Job.fromType(JobType.HUNTER));
            amount+=(amount*(Job.fromType(JobType.HUNTER).getXpGain()*pj.getLevel()));
            if(pj.getXp() > pj.getXpNeeded() || pj.getXp()+amount > pj.getXpNeeded()) {
                pj.setLevel(pj.getLevel()+1);
                pj.setXp(0);
                p.playSound(p.getLocation(), Sound.UI_TOAST_CHALLENGE_COMPLETE, SoundCategory.MASTER, 1, 1);
                new Title(p, "§6Hunting levelup", "§7Du bist jetzt Level §6" + pj.getLevel() + " §7Hunter", 30, 80, 30, 30, 80, 30);
            } else {
                pj.setXp(pj.getXp()+amount);
            }
            e.setDroppedExp(amount);
        }
    }

    /*
    * Event: Fisher
    * */
    @EventHandler
    public void fish(PlayerFishEvent e) {
        Player p = e.getPlayer();
        RpPlayer rp = RpPlayer.getPlayer(p);

        int amount = e.getExpToDrop();
        PlayerJob pj = rp.getJobs().get(Job.fromType(JobType.FISHER));
        amount+=(amount*(Job.fromType(JobType.FISHER).getXpGain()*pj.getLevel()));
        if(pj.getXp() > pj.getXpNeeded() || pj.getXp()+amount > pj.getXpNeeded()) {
            pj.setLevel(pj.getLevel()+1);
            pj.setXp(0);
            p.playSound(p.getLocation(), Sound.UI_TOAST_CHALLENGE_COMPLETE, SoundCategory.MASTER, 1, 1);
            new Title(p, "§6Fishing levelup", "§7Du bist jetzt Level §6" + pj.getLevel() + " §7Fisher", 30, 80, 30, 30, 80, 30);
        } else {
            pj.setXp(pj.getXp()+amount);
        }
        e.setExpToDrop(amount);
    }

}
