package net.xelcore.rpcore.listener;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ItemMergeEvent;

public class ItemMergeListener implements Listener {

    @EventHandler
    public void merge(ItemMergeEvent e) {
        if(e.getEntity().getItemStack().getType() == Material.GOLD_NUGGET) {
            if(e.getEntity().getItemStack().getItemMeta().getDisplayName().contains("§6Gold")) {
                e.setCancelled(true);
            }
        }
    }
}
