package net.xelcore.rpcore.listener;

import net.xelcore.rpcore.object.RpPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class PlayerChatListener implements Listener {

    @EventHandler
    public void chat(AsyncPlayerChatEvent e) {
        Player p = e.getPlayer();
        RpPlayer rp = RpPlayer.getPlayer(p);
        e.setFormat(rp.getRpClass().getColorCode() + rp.getRpClass().getName() + " §8× §8[§a" + rp.getLevel() + "§8] §7" + p.getName() + " §8» §7" + e.getMessage());
    }
}
