package net.xelcore.rpcore.listener;

import net.xelcore.rpcore.main.Main;
import net.xelcore.rpcore.object.RpPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerRespawnEvent;

public class PlayerRespawnListener implements Listener {

    @EventHandler
    public void respawn(PlayerRespawnEvent e) {
        Player p = e.getPlayer();
        RpPlayer rp = RpPlayer.getPlayer(p);
        p.setLevel(rp.getLevel());
        p.setExp(rp.mapXp());
        Main.log.debug("rpcore>PlayerInterval", "Player " + p.getName() + " respawned, restarting async thread");
        rp.startInterval();
    }
}
