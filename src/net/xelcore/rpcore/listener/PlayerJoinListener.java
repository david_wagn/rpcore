package net.xelcore.rpcore.listener;

import net.xelcore.rpcore.data.EconomyData;
import net.xelcore.rpcore.data.PlayerData;
import net.xelcore.rpcore.data.PlayerJobData;
import net.xelcore.rpcore.data.SkillData;
import net.xelcore.rpcore.main.Main;
import net.xelcore.rpcore.object.*;
import net.xelcore.rplib.message.ClickableMessage;
import net.xelcore.rplib.message.Message;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerJoinListener implements Listener {

    @EventHandler(priority = EventPriority.HIGH)
    public void join(PlayerJoinEvent e) {
        Player p = e.getPlayer();

        /* Playerdata sync */
        Main.log.debug("rpcore>PlayerData", "Syncing playerdata of " + p.getName() + "...");

        /* Loading general playerdata such as level, xp, attributes etc. */
        PlayerData pd = new PlayerData(p);
        if(!pd.exists()) {
            pd.create();
            Main.log.debug("rpcore>PlayerData", "No playerdata profile for " + p.getName() + " found, creating new one");
        }
        RpPlayer rp = new RpPlayer(p).load(pd.getValues());

        /* Loading player skills such as ability level */
        SkillData sd = new SkillData(p);
        if(!sd.exists()) {
            sd.create();
            Main.log.debug("rpcore>SkillData", "No skilldata profile for " + p.getName() + " found, creating new one");
        }
        PlayerSkills ps = new PlayerSkills(rp).load(sd.getValues());
        rp.setSkills(ps);

        /* Loading jobdata such as joblevel, xp */
        PlayerJobData pjd = new PlayerJobData(p);
        if(!pjd.exists()) {
            pjd.create();
            Main.log.debug("rpcore>JobData", "No jobdata profile for " + p.getName() + " found, creating new one");
        }
        PlayerJobs pj = new PlayerJobs(rp).load(pjd.getValues());
        rp.setJobs(pj.getJobs());

        /* Loading economydata such as networth of player */
        EconomyData ed = new EconomyData(p);
        if(!ed.exists()) {
            ed.create();
            Main.log.debug("rpcore>EconomyData", "No economydata profile for " + p.getName() + " found, creating new one");
        }
        Economy eco = new Economy(rp).load(ed.getValues());
        rp.setGold(eco.getGold());
        Main.log.debug("rpcore>PlayerData", "Datasync for " + p.getName() + " successfull");

        /* Setting default player values (Can be overwritten by setting eventPriority to "HIGHEST")*/
        rp.startInterval();
        rp.setManaRegenAmp(0.1);
        rp.setHpRegenAmp(0.0);
        rp.resetArmor();
        rp.resetMagicResistance();

        /* Setting other player values. These should not be modified by any other plugin/listener */
        p.setHealthScale(20.0);
        p.setMaxHealth(rp.getMaxHealth());
        p.setDisplayName(rp.getRpClass().getColorCode() + p.getName());
        p.setPlayerListName(rp.getRpClass().getColorCode() + rp.getRpClass().getName() + " §8× §7" + p.getName());
        p.setLevel(rp.getLevel());
        p.setExp(rp.mapXp());

        /* Loading player passives */
        for(Ability a : rp.getRpClass().getAbilities()) {
            if(a.getAbilityType() == AbilityType.PASSIVE) {
                rp.getPassiveAbilities().add(a);
            }
        }

        /* Saving time of playerjoin for limitation of playtime*/
        rp.setJoined(System.currentTimeMillis()/1000);

        e.setJoinMessage("§a» §7" + p.getName());
        p.sendMessage("§6§lTipp! §7Auf §ahttps://dwagner.one/rp/ §7findest du alle Informationen zu neuen Mobs, Items und Weiterem");
    }
}
