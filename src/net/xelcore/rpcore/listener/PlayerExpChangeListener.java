package net.xelcore.rpcore.listener;

import net.xelcore.rpcore.event.PlayerLevelupEvent;
import net.xelcore.rpcore.object.RpPlayer;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerExpChangeEvent;

public class PlayerExpChangeListener implements Listener {

    @EventHandler
    public void expchange(PlayerExpChangeEvent e) {
        Player p = e.getPlayer();
        RpPlayer rp = RpPlayer.getPlayer(p);
        int amount = Math.toIntExact(Math.round(rp.getXpMultiplier() > 0 ? (e.getAmount() * rp.getXpMultiplier()) : e.getAmount()));
        e.setAmount(0);
        if(rp.getXp() > rp.xpNeeded()) {
            Bukkit.getPluginManager().callEvent(new PlayerLevelupEvent(p, rp, rp.getLevel(), rp.getLevel()+1));
        } else {
            rp.setXp(rp.getXp()+amount);
        }
        p.setExp(rp.mapXp());

    }
}
