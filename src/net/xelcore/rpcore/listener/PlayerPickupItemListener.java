package net.xelcore.rpcore.listener;

import net.xelcore.rpcore.object.RpPlayer;
import net.xelcore.rplib.message.Message;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerPickupItemEvent;

public class PlayerPickupItemListener implements Listener {

    @EventHandler
    public void pickup(PlayerPickupItemEvent e) {
        Player p = e.getPlayer();
        RpPlayer rp = RpPlayer.getPlayer(p);

        if(e.getItem().getItemStack().getType() == Material.GOLD_NUGGET) {
            if(e.getItem().getItemStack().getItemMeta().getDisplayName().equals("§6Gold")) {
                e.setCancelled(true);
                Bukkit.getEntity(e.getItem().getUniqueId()).remove();
                rp.addGold(e.getItem().getItemStack().getAmount());
                p.playSound(p.getLocation(), Sound.ENTITY_ITEM_PICKUP, 1, 1);
                new Message(p).prefix("Du hast §6" + e.getItem().getItemStack().getAmount() + "g §7aufgeboben");
            }
        }
    }
}
