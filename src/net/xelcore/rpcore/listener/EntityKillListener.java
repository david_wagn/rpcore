package net.xelcore.rpcore.listener;

import net.xelcore.rpcore.main.Utils;
import net.xelcore.rplib.itemstack.ItemStackBuilder;
import org.bukkit.Material;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import org.bukkit.entity.Slime;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;

public class EntityKillListener implements Listener {

    @EventHandler
    public void kill(EntityDeathEvent e) {
        if (e.getEntity().getKiller() instanceof Player) {
            if (e.getEntity() instanceof Monster || e.getEntity() instanceof Slime) {
                e.getDrops().add(new ItemStackBuilder(Material.GOLD_NUGGET).setDisplayName("§6Gold").setAmount(Utils.randomNumberRange(0, 5)).build());
            }
        }
    }
}
