package net.xelcore.rpcore.listener;

import net.xelcore.rpcore.data.EconomyData;
import net.xelcore.rpcore.data.PlayerData;
import net.xelcore.rpcore.data.PlayerJobData;
import net.xelcore.rpcore.main.Main;
import net.xelcore.rpcore.object.PlayerSkills;
import net.xelcore.rpcore.object.RpPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerQuitListener implements Listener {

    @EventHandler
    public void quit(PlayerQuitEvent e) {
        Player p = e.getPlayer();
        PlayerData pd = new PlayerData(p);
        Main.log.debug("rpcore>PlayerData", "Saving playerdata of " + p.getName() + "...");
        RpPlayer rp = RpPlayer.getPlayer(p);

        PlayerSkills ps = new PlayerSkills(rp);
        ps.save();
        Main.log.debug("rpcore>PlayerData", "Skilldata of " + p.getName() + " successfully saved, deleting local obect");
        PlayerSkills.skillsList.remove(rp);

        PlayerJobData pjd = new PlayerJobData(p);
        pjd.save(rp);
        Main.log.debug("rpcore>PlayerData", "JobData of " + p.getName() + " successfully saved, deleting local object");
        rp.getJobs().clear();

        EconomyData ed = new EconomyData(p);
        ed.save(rp);
        Main.log.debug("rpcore>EconomyData", "Economydata of " + p.getName() + " successfully saved, deleting local object");

        rp.save();
        Main.log.debug("rpcore>PlayerData", "Playerdata of " + p.getName() + " successfully saved, deleting local object");
        RpPlayer.getPlayerList().remove(p.getUniqueId().toString());

        Main.log.debug("rpcore>PlayerData", "Saving complete!");

        e.setQuitMessage("§c« §7" + p.getDisplayName());
    }
}
