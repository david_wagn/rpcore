package net.xelcore.rpcore.listener;

import net.xelcore.rpcore.event.PlayerToggleAbilitySelectionEvent;
import net.xelcore.rpcore.object.RpPlayer;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerSwapHandItemsEvent;

public class PlayerSwapItemsListener implements Listener {

    @EventHandler
    public void swapitems(PlayerSwapHandItemsEvent e) {
        Player p = e.getPlayer();
        RpPlayer rp = RpPlayer.getPlayer(p);
        if(!p.isSneaking()) {
            e.setCancelled(true);
            Bukkit.getPluginManager().callEvent(new PlayerToggleAbilitySelectionEvent(p, rp, p.getInventory().getHeldItemSlot()));
        }
    }
}
