package net.xelcore.rpcore.listener;

import net.xelcore.rpcore.main.Main;
import net.xelcore.rpcore.main.Utils;
import net.xelcore.rpcore.object.RpPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

public class PlayerDamageListener implements Listener {

    @EventHandler
    public void damage(EntityDamageEvent e) {
        if(e.getEntity() instanceof Player) {
            Player p = (Player) e.getEntity();
            RpPlayer rp = RpPlayer.getPlayer(p);
            if(e.getCause() != EntityDamageEvent.DamageCause.MAGIC) {
                Main.log.debug("rpcore>DamageListener", p.getName() + " took " + Utils.format(e.getDamage()) + " physical damage from " + e.getCause().toString().toLowerCase());
                Main.log.debug("rpcore>DamageListener", "Final damage: " + Utils.format(e.getDamage()-rp.getDamageReduction(e.getDamage())) + ", reduced " + Utils.format(rp.getDamageReduction(e.getDamage())) + " (" + Utils.format(rp.getArmor()) + " Armor)");
                e.setDamage(e.getDamage()-rp.getDamageReduction(e.getDamage()));
            } else {
                e.setDamage(e.getDamage()-rp.getMagicDamageReduction(e.getDamage()));
                Main.log.debug("rpcore>DamageListener", p.getName() + " took " + Utils.format(e.getDamage()) + " magic damage");
                Main.log.debug("rpcore>DamageListener", "Final damage: " + Utils.format(e.getDamage()-rp.getMagicDamageReduction(e.getDamage())) + ", reduced " + Utils.format(rp.getMagicDamageReduction(e.getDamage())) + " (" + Utils.format(rp.getMagicResistance()) + "% magic resistance)");
            }
        }
    }
}
