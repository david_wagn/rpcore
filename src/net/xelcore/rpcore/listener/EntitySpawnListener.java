package net.xelcore.rpcore.listener;

import org.bukkit.Material;
import org.bukkit.entity.Item;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntitySpawnEvent;

public class EntitySpawnListener implements Listener {

    @EventHandler
    public void spawn(EntitySpawnEvent e) {
        if(e.getEntity() instanceof Item) {
            Item i = (Item) e.getEntity();
            if(i.getItemStack().getType() == Material.GOLD_NUGGET) {
                if(i.getItemStack().getItemMeta().getDisplayName().contains("§6Gold")) {
                    i.setCustomName("§6" + i.getItemStack().getAmount() + " Gold");
                    i.setCustomNameVisible(true);
                }
            }
        }
    }
}
