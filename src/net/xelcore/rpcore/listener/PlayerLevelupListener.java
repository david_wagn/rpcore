package net.xelcore.rpcore.listener;

import net.xelcore.rpcore.event.PlayerLevelupEvent;
import net.xelcore.rpcore.object.RpPlayer;
import net.xelcore.rplib.message.Message;
import net.xelcore.rplib.message.Title;
import org.bukkit.Sound;
import org.bukkit.SoundCategory;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class PlayerLevelupListener implements Listener {

    @EventHandler
    public void levelup(PlayerLevelupEvent e) {
        Player p = e.getPlayer();
        RpPlayer rp = e.getRpPlayer();

        rp.setXp(rp.getXp()-rp.xpNeeded());
        if(e.getPrevLevel() == 0) {
            rp.setClassPoints(rp.getClassPoints()+1);
            new Message(p).prefix("Du hast §61 §7Klassenpunkt erhalten");
            new Message(p).prefix("Du kannst jetzt mit §6/class §7deine Klasse auswählen");
        } else {
            if(rp.getLevel() % 2 != 0) {
                rp.setSkillPoints(rp.getSkillPoints()+1);
                new Message(p).prefix("Du hast §61 §7Skillpunkt erhalten");
                new Message(p).prefix("Du kannst ihn bei §6/skills §7einsetzen");
            }
        }
        rp.levelUp();
        p.setLevel(rp.getLevel());
        p.playSound(p.getLocation(), Sound.UI_TOAST_CHALLENGE_COMPLETE, SoundCategory.VOICE, 1, 1);
        new Title(p, "§6✪ §aLevelup §6✪", "§7Du bist jetzt Level §6" + e.getNextLevel(), 30, 80, 30, 30, 80, 30);
    }
}
