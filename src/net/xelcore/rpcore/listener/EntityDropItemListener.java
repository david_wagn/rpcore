package net.xelcore.rpcore.listener;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDropItemEvent;

import java.util.Collections;
import java.util.UUID;

public class EntityDropItemListener implements Listener {

    @EventHandler
    public void drop(EntityDropItemEvent e) {
        if(e.getItemDrop().getItemStack().getType() == Material.GOLD_NUGGET) {
            if(e.getItemDrop().getItemStack().getItemMeta().getDisplayName().equals("§6Gold")) {
                e.getItemDrop().getItemStack().getItemMeta().setLore(Collections.singletonList(UUID.randomUUID().toString()));
                e.getItemDrop().setCustomNameVisible(true);
                e.getItemDrop().setCustomName("§6" + e.getItemDrop().getItemStack().getAmount() + " §6Gold");
            }
        }
    }
}
