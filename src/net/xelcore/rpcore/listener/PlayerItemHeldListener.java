package net.xelcore.rpcore.listener;

import net.xelcore.rpcore.main.Main;
import net.xelcore.rpcore.main.Utils;
import net.xelcore.rpcore.object.Ability;
import net.xelcore.rpcore.object.RpPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerItemHeldEvent;

public class PlayerItemHeldListener implements Listener {

    @EventHandler
    public void itemheld(PlayerItemHeldEvent e) {
        Player p = e.getPlayer();
        RpPlayer rp = RpPlayer.getPlayer(p);

        if (rp.isCastingAbility()) {
            e.setCancelled(true);
            if (e.getNewSlot() != p.getInventory().getHeldItemSlot()) {
                if(rp.getCastableAbilities().get(e.getNewSlot()) != null) {
                    Ability a = rp.getCastableAbilities().get(e.getNewSlot());
                    Main.log.debug("rpcore>AbilitCaster", "Player casts ability " + a.getName());
                    a.execute(e.getPlayer(), Utils.getNearestEntityInSight(p, 4), p.getLocation());
                } else {
                    Main.log.debug("rpcore>AbilityCaster", "No ability on this slot");
                }
            }
        }
    }
}
