package net.xelcore.rpcore.inventory;

import net.xelcore.rpcore.main.Utils;
import net.xelcore.rpcore.object.BaseClass;
import net.xelcore.rpcore.object.RpClass;
import net.xelcore.rpcore.object.RpPlayer;
import net.xelcore.rplib.inventory.InventoryBuilder;
import net.xelcore.rplib.itemstack.ItemStackBuilder;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import java.util.*;

public class ClassInventory {

    private Player player;
    private BaseClass baseClass;
    private RpClass rpClass;
    private HashMap<BaseClass, Integer> baseClassColumn;
    private HashMap<Integer, BaseClass> columnBaseClass;
    private HashMap<RpClass, Integer> rpClassColumn;
    private HashMap<Integer, RpClass> columnRpClass;

    public ClassInventory(Player p) {
        this.player = p;
        baseClassSelector();
    }

    public void baseClassSelector() {
        new InventoryBuilder(this.player, "§8» §3Basisklassenauswahl", 3) {

            @Override
            public void onLoad() {
                fillInventory(new ItemStackBuilder(Material.GRAY_STAINED_GLASS_PANE).setDisplayName(" ").build());
                int slot = 3;
                baseClassColumn = new HashMap<>();
                columnBaseClass = new HashMap<>();
                for(int i = 1; i < BaseClass.classList.size(); i++) {
                    BaseClass bc = BaseClass.classList.get(i);
                     addItem(2, slot, new ItemStackBuilder(bc.getMaterial()).setDisplayName(bc.getColorCode() + bc.getName()).setLore(Arrays.asList(" ", "§7Primärattribut", bc.getColorCode() + Utils.formatAttribute(bc.getPrimaryAttribute()))).build());
                     baseClassColumn.put(bc, slot);
                     columnBaseClass.put(slot, bc);
                     slot+=2;
                }
            }

            @Override
            public void onItemClick(InventoryClickEvent e, ItemStack stack) {
                for(Integer slot : baseClassColumn.values()) {
                    if(isTheSameItem(2, slot, stack)) {
                        baseClass = columnBaseClass.get(slot);
                    }
                }
                classSelector();
            }
        };
    }

    public void classSelector() {
        new InventoryBuilder(this.player, "§8» §3Klassenauswahl", 3) {

            @Override
            public void onLoad() {
                fillInventory(new ItemStackBuilder(Material.GRAY_STAINED_GLASS_PANE).setDisplayName(" ").build());
                addItem(3, 1, new ItemStackBuilder(Material.IRON_DOOR).setDisplayName("§7Zurück").build());
                int slot = 3;
                rpClassColumn = new HashMap<>();
                columnRpClass = new HashMap<>();
                for(RpClass rc : RpClass.classList.values()) {
                    if(rc.getBaseClass() == baseClass) {
                        ArrayList<String> lore = new ArrayList<>();
                        lore.add(" ");
                        String[] desc = rc.getDescription().split(".;");
                        Collections.addAll(lore, desc);
                        lore.add(" ");
                        lore.add("§7Basisattribute");
                        lore.add("§c" + rc.getBaseStrength() + " §a" + rc.getBaseAgility() + " §b" + rc.getBaseIntelligence());
                        lore.add(" ");
                        lore.add("§7Attributszunahme");
                        lore.add("§c" + rc.getStrengthGain() + " §a" + rc.getAgilityGain() + " §b" + rc.getIntelligenceGain());
                        lore.add(" ");
                        lore.add("§7Fähigkeiten");
                        rc.getAbilities().forEach(ability -> lore.add("§7" + ability.getAbilityType().toString() + ", " + ability.getCastType().toString() + " §6" + ability.getName()));
                        addItem(2, slot, new ItemStackBuilder(rc.getMaterial()).setDisplayName(rc.getColorCode() + rc.getName()).setLore(lore).build());
                        rpClassColumn.put(rc, slot);
                        columnRpClass.put(slot, rc);
                        slot++;
                    }
                }

            }

            @Override
            public void onItemClick(InventoryClickEvent e, ItemStack stack) {
                if(isTheSameItem(3, 1, stack)) {
                    getGui(player, 1).openInventory();
                } else {
                    for(Integer slot : rpClassColumn.values()) {
                        if(isTheSameItem(2, slot, stack)) {
                            rpClass = columnRpClass.get(slot);
                            confirm();
                        }
                    }
                }
            }
        };
    }

    public void confirm() {
        new InventoryBuilder(this.player, "§8» §7Klassenauswahl bestätigen", 3) {

            @Override
            public void onLoad() {
                fillInventory(new ItemStackBuilder(Material.GRAY_STAINED_GLASS_PANE).setDisplayName(" ").build());
                addItem(2, 3, new ItemStackBuilder(Material.GREEN_CONCRETE).setDisplayName("§aBestätigen").build());
                addItem(2, 7, new ItemStackBuilder(Material.RED_CONCRETE).setDisplayName("§cAbbrechen").build());
            }

            @Override
            public void onItemClick(InventoryClickEvent e, ItemStack stack) {
                if(isTheSameItem(2, 3, stack)) {
                    closeInventory();
                    RpPlayer rp = RpPlayer.getPlayer(player);
                    rp.setClassPoints(rp.getClassPoints()-1);
                    rp.setRpClass(rpClass);
                } else if(isTheSameItem(2, 7, stack)) {
                    getGui(player, 1).openInventory();
                }
            }
        };
    }

}
