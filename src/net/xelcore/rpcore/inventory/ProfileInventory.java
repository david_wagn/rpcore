package net.xelcore.rpcore.inventory;

import net.xelcore.rpcore.data.PlayerJobData;
import net.xelcore.rpcore.main.Utils;
import net.xelcore.rpcore.object.*;
import net.xelcore.rplib.inventory.InventoryBuilder;
import net.xelcore.rplib.itemstack.ItemStackBuilder;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class ProfileInventory {

    public ProfileInventory(Player p, Player target) {
        RpPlayer rp = RpPlayer.getPlayerList().get(Utils.uuid(target));
        new InventoryBuilder(p, p.getName().equals(target.getName()) ? "§8» §7Profil" : "§8» §7Profil von §3" + target.getName(), 6) {

            @Override
            public void onLoad() {
                fillInventory(new ItemStackBuilder(Material.GRAY_STAINED_GLASS_PANE).setDisplayName(" ").build());
                addItem(1, 4, new ItemStackBuilder(Material.NETHER_STAR).setDisplayName("§7Skillpunkte§8: §6" + rp.getSkillPoints()).setAmount(rp.getSkillPoints() == 0 ? 1 : rp.getSkillPoints()).build());
                addItem(1, 5, new ItemStackBuilder(rp.getRpClass().getMaterial()).setDisplayName("§6" + rp.getRpClass().getName()).setLore(Arrays.asList(" ", "§7Basisklasse", "§8- §3" + rp.getRpClass().getBaseClass().getName())).build());
                addItem(1, 6, new ItemStackBuilder(Material.FIREWORK_STAR).setDisplayName("§7Klassenpunkte§8: §6" + rp.getClassPoints()).setAmount(rp.getClassPoints() == 0 ? 1 : rp.getClassPoints()).build());
                addItem(2, 4, new ItemStackBuilder(Material.RED_CONCRETE).setDisplayName(rp.getRpClass().getPrimaryAttribute() == Attribute.STRENGTH ? "§4Stärke §8- §6Primärattribut" : "§4Stärke").setLore(Arrays.asList(" ", "§7Aktueller Wert", "§8- §4" + Utils.format(rp.getStrength()), " ", "§7Zunahme pro Level", "§8- §4" + rp.getRpClass().getStrengthGain(), " ", "§8= §c" + Utils.format(p.getMaxHealth()) + " Maximale Leben", "§8= §5" + Utils.format(rp.getMagicResistance()) + "% Magieresistenz")).build());
                addItem(2, 5, new ItemStackBuilder(Material.LIME_CONCRETE).setDisplayName(rp.getRpClass().getPrimaryAttribute() == Attribute.AGILITY ? "§aAgilität §8- §6Primärattribut" : "§aAgilität").setLore(Arrays.asList(" ", "§7Aktueller Wert", "§8- §a" + Utils.format(rp.getAgility()), " ", "§7Zunahme pro Level", "§8- §a" + rp.getRpClass().getAgilityGain(), " ", "§8= §7" + Utils.format(rp.getArmor()) + " Rüstung")).build());
                addItem(2, 6, new ItemStackBuilder(Material.LIGHT_BLUE_CONCRETE).setDisplayName(rp.getRpClass().getPrimaryAttribute() == Attribute.INTELLIGENCE ? "§bIntelligenz §8- §6Primärattribut" : "§bIntelligenz").setLore(Arrays.asList(" ", "§7Aktueller Wert", "§8- §b" + Utils.format(rp.getIntelligence()), " ", "§7Zunahme pro Level", "§8- §b" + rp.getRpClass().getIntelligenceGain(), " ", "§8= §7" + Utils.format(rp.getMaxMana()) + " Maximales Mana", "§8= §7" + Utils.format(rp.getManaRegen()*10.0) + " Manaregeneration pro Sekunde")).build());

                StringBuilder progress = new StringBuilder();
                int c;
                for(c = 0; c < Math.abs(rp.getLevelProgress()/10); c++) {
                    progress.append("§e░");
                }

                while(c < 10) {
                    c++;
                    progress.append("§7░");
                }

                addItem(3, 5, new ItemStackBuilder(Material.EXPERIENCE_BOTTLE).setDisplayName("§6Level " + rp.getLevel()).setLore(Arrays.asList(" ", "§7Fortschritt §6" + rp.getXp() + "§8/§7" + rp.xpNeeded(), "§8" + progress)).build());

                int column = 2;
                int row = 5;
                for(Job j : Job.jobList.values()) {
                    ArrayList<String> lore = new ArrayList<>();
                    PlayerJob pj = rp.getJobs().get(j);
                    lore.add(" ");
                    lore.add("§7Fortschritt");
                    lore.add("§6" + pj.getXp() + "§8/§7" + pj.getXpNeeded());
                    StringBuilder lp = new StringBuilder();
                    int b;
                    for(b = 0; b < Math.abs(pj.getLevelProgress()/10); b++) {
                        lp.append("§e░");
                    }

                    while(b < 10) {
                        b++;
                        lp.append("§7░");
                    }
                    lore.add("§7" + lp);
                    lore.add(" ");
                    lore.add("§7Du erhältst " + Utils.format(pj.getJob().getXpGain()*(pj.getLevel() == 0 ? 1 : pj.getLevel())) + "% mehr xp");
                    lore.add("§7durch das Ausführen des Berufs");
                    lore.add(" ");
                    String[] desc = pj.getJob().getDescription().split(".;");
                    Collections.addAll(lore, desc);
                    lore.add(" ");
                    addItem(row, column, new ItemStackBuilder(j.getMaterial()).setDisplayName("§8(§a" + pj.getLevel() + "§8) §6" + j.getName()).setLore(lore).build());
                    column++;
                    if(column > 8) {
                        column = 2;
                        row+=1;
                    }
                }
            }

            @Override
            public void onItemClick(InventoryClickEvent inventoryClickEvent, ItemStack itemStack) {

            }
        };
    }
}
