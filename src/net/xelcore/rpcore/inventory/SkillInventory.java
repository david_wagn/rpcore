package net.xelcore.rpcore.inventory;

import net.xelcore.rpcore.main.Main;
import net.xelcore.rpcore.object.Ability;
import net.xelcore.rpcore.object.RpPlayer;
import net.xelcore.rplib.inventory.InventoryBuilder;
import net.xelcore.rplib.itemstack.ItemStackBuilder;
import net.xelcore.rplib.message.Message;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import java.util.*;

public class SkillInventory {

    private HashMap<Ability, Integer> abilitySlot;

    public SkillInventory(Player p) {
        RpPlayer rp = RpPlayer.getPlayer(p);
        new InventoryBuilder(p, "§8» §3Skills", 4) {

            @Override
            public void onLoad() {
                fillInventory(new ItemStackBuilder(Material.GRAY_STAINED_GLASS_PANE).setDisplayName(" ").build());
                addItem(1, 1, new ItemStackBuilder(Material.NETHER_STAR).setDisplayName("§7Skillpunkte§8: §6" + rp.getSkillPoints()).setAmount(rp.getSkillPoints() == 0 ? 1 : rp.getSkillPoints()).build());
                abilitySlot = new HashMap<>();
                int count = 3;
                for (Ability a : rp.getRpClass().getAbilities()) {
                    ArrayList<String> lore = new ArrayList<>();
                    lore.add(" ");
                    String[] desc = a.getDescription().split(".;");
                    Collections.addAll(lore, desc);
                    lore.add(" ");
                    lore.add("§7Type: " + a.getAbilityType().toString());
                    lore.add("§7Casttype: " + a.getCastType().toString());
                    lore.add(" ");
                    lore.add("§7Wert");
                    String val;
                    String[] split = a.getValue().split("/");

                    if(rp.getSkills().get(a) == 1) {
                        split[0] = "§6" + split[0];
                    } else if(rp.getSkills().get(a) == 2) {
                        split[1] = "§6" + split[1];
                    } else if(rp.getSkills().get(a) == 3) {
                        split[2] = "§6" + split[2];
                    } else if(rp.getSkills().get(a) == 4) {
                        split[3] = "§6" + split[3];
                    } else if(rp.getSkills().get(a) == 5) {
                        split[4] = "§6" + split[4];
                    }

                    split[0] = "§7" + split[0] + "§7/";
                    split[1] = "§7" + split[1] + "§7/";
                    split[2] = "§7" + split[2] + "§7/";
                    split[3] = "§7" + split[3] + "§7/";
                    split[4] = "§7" + split[4] + "";
                    val = split[0] + split[1] + split[2] + split[3] + split[4];
                    lore.add(val);
                    if (a.getManacost() != 0) {
                        lore.add("");
                        lore.add("§7Manakosten");
                        lore.add("§b" + a.getManacost());
                    }
                    if (a.getDuration() != 0) {
                        lore.add("");
                        lore.add("§7Dauer");
                        lore.add("§9" + a.getDuration());
                    }
                    if (a.getCooldown() != 0) {
                        lore.add(" ");
                        lore.add("§7Cooldown");
                        lore.add("§a" + a.getCooldown());

                    }
                    addItem(2, count, new ItemStackBuilder(Material.MAP).setDisplayName("§6" + a.getName()).setLore(lore).build());
                    String name;
                    Material material;
                    ArrayList<String> l = new ArrayList<>();
                    if(rp.getSkills().get(a) == 0) {
                        l.add(" ");
                        name = "§8(§a" + a.getId() + "§8) §cNoch nicht geskillt";
                        material = Material.RED_CONCRETE;
                    } else {
                        name = "§8(§a" + a.getId() + "§8) §7Level §6" + rp.getSkills().get(a);
                        material = Material.GREEN_CONCRETE;
                    }
                    StringBuilder level = new StringBuilder();
                    int c = 0;
                    for(int i = 0; i < rp.getSkills().get(a); i++) {
                        level.append("§6⬛");
                        c++;
                    }
                    while(c < 5) {
                        level.append("§7⬜");
                        c++;
                    }
                    l.add(level.toString());
                    l.add(" ");
                    if(rp.getSkills().get(a) == 5) {
                        l.add("§7Maximales Level erreicht");
                        material = Material.YELLOW_CONCRETE;
                    } else if(rp.getSkillPoints() != 0) {
                        l.add("§7Klicke, um zu verbessern");
                    } else {
                        l.add("§cNicht genügend Skillpunkte zum verbessern");
                    }
                    abilitySlot.put(a, count);
                    addItem(3, count, new ItemStackBuilder(material).setDisplayName(name).setLore(l).build());

                    count++;
                }
            }

            @Override
            public void onItemClick(InventoryClickEvent e, ItemStack stack) {
                if(rp.getSkillPoints() == 0) {
                    closeInventory();
                    new Message(p).errorprefix("Du hast nicht genügend Skillpunkte");
                    return;
                }

                for(Ability a : rp.getRpClass().getAbilities()) {
                    if(isTheSameItem(3, abilitySlot.get(a), stack)) {
                        if(rp.getSkills().get(a) < 5) {
                            int level = rp.getSkills().get(a);
                            Main.log.debug("rpcore>PlayerSkills", "Player " + p.getName() + " leveled up ability " + a.getName() + ", its now level " + (level+1));
                            rp.getSkills().remove(a);
                            rp.getSkills().put(a, level+1);
                            rp.setSkillPoints(rp.getSkillPoints()-1);
                            p.playSound(p.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1, 1);
                            openInventory();
                        }
                        break;
                    }
                }
            }
        };
    }
}
